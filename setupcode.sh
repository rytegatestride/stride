#accept database credentials
echo "Enter your database name"
read dbname
echo "Your entered: $dbname"
echo "Enter username"
read username
echo "You entered $username"
echo "Enter your database password > "
read -s dbpassword
#initialize git repository
git init
#pull git code
git remote add origin https://myrealjay@bitbucket.org/myrealjay/opsmanagernew.git
git pull origin master
#install dependcies
composer install --no-dev --no-interaction --prefer-dist
#set environment variables
cp .env.example .env
php artisan key:generate
php artisan jwt:secret
php artisan env:set DB_DATABASE $dbname
php artisan env:set DB_USERNAME $username
php artisan env:set DB_PASSWORD $dbpassword
composer dump-autoload
#run migrations
php artisan migrate --force
php artisan db:seed --class=NewSetupSeeder
php artisan cache:clear
php artisan config:clear
php artisan config:cache