<?php

namespace App\Rules;

use App\Attendance;

use Illuminate\Contracts\Validation\Rule;

class CheckAttendanceRule implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if($value=='others'){
            return true;
        }
        else{
            $att=Attendance::find($value);
            if($att){
                return true;
            }
            return false;
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return ' must be filled';
    }
}
