<?php

namespace App\Rules;
use App\PayPeriod;

use Illuminate\Contracts\Validation\Rule;

class CheckBenefitRule implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if($value=='no end'){
            return true;
        }
        else{
            if(PayPeriod::where('id',$value)->first()){
                return true;
            }
            return false;
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The End Payperiod can only be a valid payperiod or no end';
    }
}
