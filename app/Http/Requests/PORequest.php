<?php

namespace App\Http\Requests;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;

class PORequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    protected function failedValidation(Validator $validator)
    {
        $messages=implode('</br>',$validator->messages()->all());
        throw new HttpResponseException(response()->json(['error'=>$messages], 422));
    }

    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'project_id'=>'required|integer|exists:projects,id',
            'user_id'=>'required|integer|exists:users,id',
            'individual_project_phase_id'=>'required|integer|exists:individual_project_phases,id',
            'date_needed'=>'required|date',
            'note'=>'required|string',
            'ticket_id'=>'nullable|integer|exists:tickets,id'
        ];
    }
}
