<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\Appraisal::class,
        Commands\Utility::class,
        Commands\Invoice::class,
        Commands\Incident::class,
        Commands\Notification::class,
        Commands\CreateProduct::class,
        Commands\UpdateSalary::class,
        Commands\ChangeStatus::class,
        Commands\InvoiceStatusUpdater::class,
        Commands\ImortantDatesAlert::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('birthday:display')
        ->dailyAt('06:00');
        $schedule->command('appraise:day')
        ->dailyAt('06:00');
        $schedule->command('utility:alert')
        ->dailyAt('06:00');
        $schedule->command('invoice:notify')
        ->dailyAt('06:00');
        $schedule->command('incident:remind')
        ->dailyAt('06:00');
        $schedule->command('product:create')
        ->dailyAt('06:00');
        $schedule->command('salary:update')
        ->dailyAt('06:00');
        $schedule->command('status:change')
        ->dailyAt('06:00');
        $schedule->command('invoice:update_status')
        ->dailyAt('06:00');
        $schedule->command('important_dates:alert')
        ->dailyAt('06:00');
        
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
