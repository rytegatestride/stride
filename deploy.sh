php artisan down
git pull origin master
#install dependcies
composer install --no-dev --no-interaction --prefer-dist
composer dump-autoload
#run migrations
php artisan migrate --force
#clear config
php artisan cache:clear
php artisan config:clear
php artisan config:cache
php artisan up