<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Invoice;
use Faker\Generator as Faker;

$factory->define(Invoice::class, function (Faker $faker) {
    return [
        'client'=>1,
        'project'=>1,
        'owner'=>1,
        'start_date'=>date('Y-m-d'),
        'due_date'=>date('Y-m-d')
    ];
});
