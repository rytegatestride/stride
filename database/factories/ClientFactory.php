<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Client;
use Faker\Generator as Faker;

$factory->define(Client::class, function (Faker $faker) {
    return [
        'firstname'=>$faker->name,
        'lastname'=>$faker->name,
        'company'=>$faker->sentence
    ];
});
