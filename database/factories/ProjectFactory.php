<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Project;
use Faker\Generator as Faker;

$factory->define(Project::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'start_date' => date('Y-m-d'),
        'division_id' => 1,
        'department_id'=>1
    ];
});
