<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAddTaxToVendorContracts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vendor_contracts',function($table){
            $table->tinyInteger('add_tax')->default('0');
            $table->integer('inventory_location_id')->unsigned()->nullable();
            $table->double('shipping_fee')->nullable();
            $table->string('address',500)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vendor_contracts',function($table){
            $table->dropColumn('add_tax');
            $table->dropColumn('inventory_location_id');
            $table->dropColumn('shipping_fee');
            $table->dropColumn('address',500);
        });
    }
}
