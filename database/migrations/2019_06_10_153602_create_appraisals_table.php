<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppraisalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appraisals', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('appraisal_base_id')->unsigned();
            $table->integer('kpiposition_id')->unsigned()->nullable();
            $table->string('comment',800)->nullable();
            $table->integer('usergoal_id')->unsigned()->nullable();
            $table->foreign('appraisal_base_id')->references('id')->on('appraisal_bases');
            $table->foreign('kpiposition_id')->references('id')->on('kpipositions');
            $table->foreign('usergoal_id')->references('id')->on('user_goals');
            $table->double('score');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appraisals');
    }
}
