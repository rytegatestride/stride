<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRolesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('roles', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('permission_id')->nullable()->default(0);
            $table->integer('subscription_id')->nullable()->default(0);
            $table->string('name');
            $table->tinyInteger('active')->unsigned();
            $table->timestamps();

            // $table->foreign('permission_id')->references('id')->on('permissions');
            // $table->foreign('subscription_id')->references('id')->on('subscriptions');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('roles');
    }
}
