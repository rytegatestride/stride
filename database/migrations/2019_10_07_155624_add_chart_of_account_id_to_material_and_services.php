<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddChartOfAccountIdToMaterialAndServices extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('material_and_services',function($table){
            $table->integer('material_and_service_id')->unsigned()->nullable();
            $table->foreign('material_and_service_id')->references('id')->on('material_and_services');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('material_and_services',function($table){
            $table->dropColumn('material_and_service_id');
        });
    }
}
