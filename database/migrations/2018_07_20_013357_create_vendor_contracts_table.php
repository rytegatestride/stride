<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVendorContractsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vendor_contracts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('vendor_id')->unsigned();
            $table->integer('material_and_service_id')->unsigned()->nullable();
            $table->integer('liquid_asset_id')->unsigned()->nullable();
            $table->integer('project_id')->unsigned();
            $table->integer('individual_project_phase_id')->unsigned();
            $table->integer('project_labour_id')->unsigned()->nullable();
            $table->text('amount');
            $table->text('start_date');
            $table->text('end_date');
            $table->integer('contractor');
            $table->integer('created_by');
            $table->text('notes');
            $table->integer('status')->default(0);
            $table->timestamps();

            $table->foreign('vendor_id')->references('id')->on('vendors');
            $table->foreign('material_and_service_id')->references('id')->on('material_and_services');
            $table->foreign('liquid_asset_id')->references('id')->on('liquid_assets');
            $table->foreign('project_id')->references('id')->on('projects');
            $table->foreign('individual_project_phase_id')->references('id')->on('individual_project_phases');
            $table->foreign('project_labour_id')->references('id')->on('project_labours');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vendor_contracts');
    }
}
