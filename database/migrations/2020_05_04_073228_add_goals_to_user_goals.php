<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddGoalsToUserGoals extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_goals',function($table){
            $table->integer('individualgoal_id')->unsigned()->nullable()->change();
            $table->integer('departmentalgoal_id')->unsigned()->nullable();
            $table->integer('subgoal_id')->unsigned()->nullable();
            $table->tinyInteger('active')->default('1');
            $table->integer('carlendaryear_id')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_goals',function($table){
            $table->dropColumn('departmentalgoal_id');
            $table->dropColumn('subgoal_id');
            $table->dropColumn('carlendaryear_id');
        });
    }
}
