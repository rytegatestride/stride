<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDateDeliveredToClientProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('client_products', function (Blueprint $table) {
            $table->timestamp('date_delivered')->nullable();
        });
        Schema::table('pending_inventory_withdrawals', function (Blueprint $table) {
            $table->integer('client_product_id')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('client_products', function (Blueprint $table) {
            $table->dropColumn('date_delivered');
        });
        Schema::table('pending_inventory_withdrawals', function (Blueprint $table) {
            $table->dropColumn('client_product_id');
        });
    }
}
