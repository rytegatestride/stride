<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPurchaseOrderMaterialIdToBankAccountTopups extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bank_account_topups',function($table){
            $table->integer('purchase_order_material_id')->unsigned()->nullable();
            $table->integer('purchase_order_service_id')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bank_account_topups',function($table){
            $table->dropColumn('purchase_order_material_id');
            $table->dropColumn('purchase_order_service_id');
        });
    }
}
