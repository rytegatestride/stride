<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUtilityBillItemTaxesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('utility_bill_item_taxes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('utility_bill_item_id')->unsigned();
            $table->double('component');
            $table->double('amount');
            $table->boolean('percentage')->default('1');
            $table->timestamps();
        });
        Schema::table('utility_bill_items',function($table){
            $table->integer('add_tax')->default('0');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('utility_bill_item_taxes');
        Schema::table('utility_bill_items',function($table){
            $table->dropColumn('add_tax');
        });
    }
}
