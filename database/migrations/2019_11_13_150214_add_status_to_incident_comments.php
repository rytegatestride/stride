<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStatusToIncidentComments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('incident_comments',function($table){
            $table->integer('initial_status')->nullable();
            $table->integer('new_status')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('incident_comments',function($table){
            $table->dropColumn('initial_status');
            $table->dropColumn('new_status');
        });
    }
}
