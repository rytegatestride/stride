<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLiquidAssetsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('liquid_assets', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('asset_model_id')->unsigned();
            $table->integer('asset_manufacturer_id')->unsigned();
            $table->integer('asset_category_id')->unsigned();
            $table->string('serial_no');
            $table->integer('color_id')->nullable();
            $table->text('maintenance')->nullable();
            $table->tinyInteger('active')->default(1);
            $table->tinyInteger('retired')->default(0);
            $table->integer('assignment_id')->nullable();
            $table->double('purchase_amount')->default(0);
            $table->text('purchase_date')->nullable();
            $table->integer('vendor_id')->unsigned()->nullable();
            $table->string('location')->nullable();
            $table->integer('city_id')->nullable();
            $table->integer('country_id')->nullable();
            $table->integer('state_id')->nullable();
            $table->timestamps();

            $table->foreign('asset_model_id')->references('id')->on('asset_models');
            $table->foreign('asset_manufacturer_id')->references('id')->on('asset_manufacturers');
            $table->foreign('asset_category_id')->references('id')->on('asset_categories');
            $table->foreign('vendor_id')->references('id')->on('vendors');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('liquid_assets');
    }
}
