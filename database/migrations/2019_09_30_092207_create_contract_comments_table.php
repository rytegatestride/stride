<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContractCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contract_comments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('initial_status')->nullable();
            $table->integer('new_status')->nullable();
            $table->string('comment',500);
            $table->integer('user_id')->unsigned();
            $table->integer('contract_id')->unsigned();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('contract_id')->references('id')->on('vendor_contracts');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contract_comments');
    }
}
