<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddInvoiceDetailIdToExpenseRecords extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('expense_records',function($table){
            $table->integer('invoice_detail_id')->unsigned()->nullable();
        });
        Schema::table('bank_account_topups',function($table){
            $table->integer('invoice_detail_id')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('expense_records',function($table){
            $table->dropColumn('invoice_detail_id');
        });
        Schema::table('bank_account_topups',function($table){
            $table->dropColumn('invoice_detail_id');
        });
    }
}
