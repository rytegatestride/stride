<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientSubCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client_sub_comments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('client_comment_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->text('comment');
            $table->foreign('client_comment_id')->references('id')->on('client_comments');
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client_sub_comments');
    }
}
