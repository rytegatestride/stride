<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeBudgetManagers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('budget_managers',function($table){
            $table->dropColumn('active');
            $table->integer('project_id')->unsigned()->nullable();
            $table->tinyInteger('budget')->default('0');
            $table->tinyInteger('po')->default('0');
            $table->tinyInteger('debit')->default('0');
            $table->tinyInteger('expenses')->default('0');
            $table->tinyInteger('ewallet')->default('0');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('budget_managers',function($table){
            $table->dropColumn('project_id');
            $table->dropColumn('budget');
            $table->dropColumn('po');
            $table->dropColumn('debit');
            $table->dropColumn('expenses');
            $table->dropColumn('ewallet');
        });
    }
}
