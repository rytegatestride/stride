<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddViewAppraisalPeriodToPermissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('permissions', function($table){
            $table->tinyInteger('view_appraisal_period')->default('0');
            $table->tinyInteger('edit_appraisal_period')->default('0');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('permissions', function($table){
            $table->dropColumn('view_appraisal_period');
            $table->dropColumn('edit_appraisal_period');
        });
    }
}
