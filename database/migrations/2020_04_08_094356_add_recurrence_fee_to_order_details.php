<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRecurrenceFeeToOrderDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('order_details',function($table){
            $table->double('recurring_fee')->nullable();
            $table->integer('recurrence')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('order_details',function($table){
            $table->dropColumn('recurring_fee');
            $table->dropColumn('recurrence');
        });
    }
}
