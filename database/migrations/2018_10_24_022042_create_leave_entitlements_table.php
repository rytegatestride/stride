<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeaveEntitlementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leave_entitlements', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('leave_type_id')->unsigned();
            $table->integer('user_id')->unsigned()->nullable();
            $table->integer('amount');
            $table->integer('management_level_id')->unsigned()->nullable();
            $table->timestamps();

            $table->foreign('leave_type_id')->references('id')->on('leave_types');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('management_level_id')->references('id')->on('management_levels');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leave_entitlements');
    }
}
