<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddClientSummaryToPermissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('permissions',function($table){
            $table->tinyInteger('client_summary')->default('0');
            $table->tinyInteger('client_details')->default('0');
            $table->tinyInteger('client_orders')->default('0');
            $table->tinyInteger('client_products')->default('0');
            $table->tinyInteger('client_transactions')->default('0');
            $table->tinyInteger('client_invoices')->default('0');
            $table->tinyInteger('client_files')->default('0');
            $table->tinyInteger('client_messages')->default('0');
            $table->tinyInteger('client_notes')->default('0');
            $table->tinyInteger('edit_client_details')->default('0');
            $table->tinyInteger('edit_client_orders')->default('0');
            $table->tinyInteger('edit_client_products')->default('0');
            $table->tinyInteger('edit_client_files')->default('0');
            $table->tinyInteger('edit_client_messages')->default('0');
            $table->tinyInteger('delete_client_files')->default('0');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
