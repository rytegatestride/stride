<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddContractTaxToExpenseRecords extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('expense_records',function($table){
            $table->tinyInteger('contract_tax')->nullable();
            $table->tinyInteger('contract_shipping')->nullable();
            $table->tinyInteger('pr_tax')->nullable();
            $table->tinyInteger('pr_shipping')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('expense_records',function($table){
            $table->dropColumn('contract_tax');
            $table->dropColumn('contract_shipping');
            $table->dropColumn('pr_tax');
            $table->dropColumn('pr_shipping');
        });
    }
}
