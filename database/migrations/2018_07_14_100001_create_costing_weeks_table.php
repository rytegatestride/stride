<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCostingWeeksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('costing_weeks', function (Blueprint $table) {
            $table->increments('id');
            $table->string('start_date');
            $table->string('end_date');
            $table->integer('priority');
            $table->integer('department_id')->unsigned();
            $table->integer('individual_project_phases_id')->unsigned();
            $table->integer('created_by');
            $table->integer('approved_by')->default(0);
            $table->integer('declined_by')->default(0);
            $table->integer('canceled_by')->default(0);
            $table->integer('review_submitter')->default(0);
            $table->integer('status')->default(0);
            $table->timestamps();

            $table->foreign('department_id')->references('id')->on('departments');
            $table->foreign('individual_project_phases_id')->references('id')->on('individual_project_phases');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('costing_weeks');
    }
}
