<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAccountNameToEmployeeImprestTopupWithdrawals extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('employee_imprest_topup_withdrawals',function($table){
            $table->string('account_name')->nullable();
            $table->string('account_number')->nullable();
            $table->string('reciepient_name')->nullable();
            $table->string('reciepient_phone')->nullable();
            $table->string('reciepient_bank')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('employee_imprest_topup_withdrawals',function($table){
            $table->dropColumn('account_name');
            $table->dropColumn('account_number');
            $table->dropColumn('reciepient_name');
            $table->dropColumn('reciepient_phone');
            $table->dropColumn('reciepient_bank');
        });
    }
}
