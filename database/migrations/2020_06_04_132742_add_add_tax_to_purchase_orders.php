<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAddTaxToPurchaseOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('purchase_orders',function($table){
            $table->boolean('add_tax')->default(false);
            $table->double('shipping_fee')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('purchase_orders',function($table){
            $table->dropColumn('add_tax');
            $table->dropColumn('shipping_fee')->nullable();
        });
    }
}
