<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddChangeInitialProjectInPendingInventoryWithdrawals extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pending_inventory_withdrawals', function (Blueprint $table) {
            $table->integer('initial_project')->unsigned()->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pending_inventory_withdrawals', function (Blueprint $table) {
            $table->dropColumn('initial_project');
        });
    }
}
