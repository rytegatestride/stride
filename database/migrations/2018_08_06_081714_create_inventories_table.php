<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInventoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inventories', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('materrial_and_service_id')->unsigned();
            $table->integer('inventory_location_id')->unsigned();
            $table->timestamps();

            $table->foreign('materrial_and_service_id')->references('id')->on('material_and_services');
            $table->foreign('inventory_location_id')->references('id')->on('inventory_locations');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inventories');
    }
}
