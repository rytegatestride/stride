<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddExcessOneToApprovedAttendances extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('approved_attendances',function($table){
            $table->float('excess_one')->nullable();
            $table->float('excess_two')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('approved_attendances',function($table){
            $table->dropColumn('excess_one');
            $table->dropColumn('excess_two');
        });
    }
}
