<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeVendorIdInPendingInventoryWithdrawals extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pending_inventory_withdrawals', function (Blueprint $table) {
            $table->integer('vendor_id')->unsigned()->nullable()->change();
            $table->integer('individual_project_phase_id')->unsigned()->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pending_inventory_withdrawals', function (Blueprint $table) {
            $table->dropColumn('vendor_id');
            $table->dropColumn('individual_project_phase_id');
        });
    }
}
