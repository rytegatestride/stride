<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoiceSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoice_settings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('daily')->default('1');
            $table->integer('weekly')->default('1');
            $table->integer('monthly')->default('1');
            $table->integer('annually')->default('1');
            $table->timestamps();
        });

        Schema::table('permissions',function($table){
            $table->tinyInteger('view_invoice_settings')->default('0');
            $table->tinyInteger('edit_invoice_settings')->default('0');
            $table->tinyInteger('view_invoice_notification')->default('0');
            $table->tinyInteger('edit_invoice_notification')->default('0');
        });

        Schema::table('subscriptions',function($table){
            $table->tinyInteger('invoice_notification')->default('0');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoice_settings');
        Schema::table('permissions',function($table){
            $table->dropColumn('view_invoice_settings');
            $table->dropColumn('edit_invoice_settings');
            $table->dropColumn('view_invoice_notification');
            $table->dropColumn('edit_invoice_notification');
        });

        Schema::table('subscriptions',function($table){
            $table->dropColumn('invoice_notification');
        });
    }
}
