<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSalesHubToPermissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('permissions', function (Blueprint $table) {
            $table->tinyInteger('my_sales_hub')->default('0');
            $table->tinyInteger('create_sales_lead')->default('0');
            $table->tinyInteger('edit_sales_lead')->default('0');
            $table->tinyInteger('view_sales_lead')->default('0');
            $table->tinyInteger('delete_sales_lead')->default('0');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('permissions', function (Blueprint $table) {
            $table->dropColumn('my_sales_hub');
            $table->dropColumn('create_sales_lead');
            $table->dropColumn('edit_sales_lead');
            $table->dropColumn('view_sales_lead');
            $table->dropColumn('delete_sales_lead');
        });
    }
}
