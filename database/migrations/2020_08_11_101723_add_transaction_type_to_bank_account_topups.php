<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTransactionTypeToBankAccountTopups extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bank_account_topups', function (Blueprint $table) {
            $table->tinyInteger('transaction_type')->default('1');
            $table->integer('pr_tax')->nullable();
            $table->integer('pr_shipping')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bank_account_topups', function (Blueprint $table) {
            $table->dropColumn('transaction_type');
            $table->dropColumn('pr_tax');
            $table->dropColumn('pr_shipping');
        });
    }
}
