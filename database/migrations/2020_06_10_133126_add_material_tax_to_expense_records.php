<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMaterialTaxToExpenseRecords extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('expense_records',function($table){
            $table->integer('material_tax')->nullable();
            $table->integer('material_shipping')->nullable();
        });
        Schema::table('bank_account_topups',function($table){
            $table->integer('material_tax')->nullable();
            $table->integer('material_shipping')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('expense_records',function($table){
            $table->dropColumn('material_tax');
            $table->dropColumn('material_shipping');
        });
        Schema::table('bank_account_topups',function($table){
            $table->dropColumn('material_tax');
            $table->dropColumn('material_shipping');
        });
    }
}
