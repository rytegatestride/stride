<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddOldStatusToInviceStatusLogs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('invoice_status_logs', function (Blueprint $table) {
            $table->bigInteger('old_status_id')->unsigned()->nullable();
            $table->integer('invoice_id')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('invoice_status_logs', function (Blueprint $table) {
            $table->dropColumn('old_status_id');
            $table->dropColumn('invoice_id');
        });
    }
}
