<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUtilityBillItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('utility_bill_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('utility_bill_id')->unsigned();
            $table->string('expense_id');
            $table->timestamp('date')->nullable();
            $table->timestamp('due_date')->nullable();
            $table->integer('status')->default('0');
            $table->integer('material_and_service_id')->unsigned()->nullable();
            $table->double('amount');
            $table->integer('individual_project_phase_id')->unsigned()->nullable();
            $table->foreign('individual_project_phase_id')->references('id')->on('individual_project_phases');
            $table->foreign('material_and_service_id')->references('id')->on('material_and_services');
            $table->foreign('utility_bill_id')->references('id')->on('utility_bills');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('utility_bill_items');
    }
}
