<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBillSeetingToPermissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('permissions',function($table){
            $table->tinyInteger('utility_bill_settings')->default('0');
            $table->tinyInteger('edit_utility_bill_settings')->default('0');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('permissions',function($table){
            $table->dropColumn('utility_bill_settings');
            $table->dropColumn('edit_utility_bill_settings');
        });
    }
}
