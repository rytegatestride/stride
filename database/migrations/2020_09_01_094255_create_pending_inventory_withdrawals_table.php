<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePendingInventoryWithdrawalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pending_inventory_withdrawals', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('project_id')->unsigned();
            $table->integer('material_and_service_id')->unsinged();
            $table->integer('vendor_id')->unsinged();
            $table->integer('individual_project_phase_id')->unsinged();
            $table->double('amount')->default('0');
            $table->timestamp('date')->nullable();
            $table->integer('created_by')->unsigned()->nullable();
            $table->text('notes')->nullable();
            $table->integer('inventory_id')->unsinged();
            $table->integer('initial_project')->unsinged();
            $table->integer('initial_phase')->unsinged();
            $table->integer('quantity')->unsinged();
            $table->integer('status')->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pending_inventory_withdrawals');
    }
}
