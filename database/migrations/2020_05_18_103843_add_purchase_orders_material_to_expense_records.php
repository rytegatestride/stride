<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPurchaseOrdersMaterialToExpenseRecords extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('expense_records',function($table){
            $table->integer('purchase_order_material_id')->unsigned()->nullable();
            $table->integer('purchase_order_service_id')->unsigned()->nullable();
            $table->foreign('purchase_order_material_id')->references('id')->on('purchase_orders_materials');
            $table->foreign('purchase_order_service_id')->references('id')->on('purchase_orders_services');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('expense_records',function($table){
            $table->dropColumn('purchase_order_material_id');
            $table->dropColumn('purchase_order_service_id');
        });
    }
}
