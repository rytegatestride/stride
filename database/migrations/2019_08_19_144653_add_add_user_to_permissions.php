<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAddUserToPermissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('permissions', function($table) {
            $table->tinyInteger('add_user')->default('0');
            $table->tinyInteger('edit_salary')->default('0');
            $table->tinyInteger('user_management')->default('0');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('permissions', function($table) {
            $table->dropColumn('add_user');
            $table->dropColumn('edit_salary');
            $table->dropColumn('user_management');
        });
    }
}
