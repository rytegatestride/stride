<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUtilityBillSubCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('utility_bill_sub_comments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('utility_bill_comment_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->string('comment',500);
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('utility_bill_comment_id')->references('id')->on('utility_bill_comments');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('utility_bill_sub_comments');
    }
}
