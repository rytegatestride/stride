<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUtilityTaxToExpenseRecords extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('expense_records',function($table){
            $table->integer('utility_tax')->nullable();
        });
        Schema::table('bank_account_topups',function($table){
            $table->integer('utility_tax')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('expense_records',function($table){
            $table->dropColumn('utility_tax');
        });
        Schema::table('bank_account_topups',function($table){
            $table->dropColumn('utility_tax');
        });
    }
}
