<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExpenseRecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('expense_records', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('project_id')->unsigned()->nullable();//yet to find out why it threw error
            $table->integer('vendor_id')->unsigned()->nullable();
            $table->integer('chart_of_account_id')->unsigned()->nullable();
            $table->integer('material_and_service_id')->unsigned()->nullable();
            $table->integer('bank_account_id')->unsigned()->nullable();
            $table->integer('individual_project_phase_id')->unsigned()->nullable();//and this as well
            // $table->integer('purchase_requisition_id')->nullable();
            $table->integer('vendor_contract_id')->unsigned()->nullable();
            $table->string('amount')->nullable();
            $table->string('date')->nullable();
            $table->integer('vendor_guarantor')->nullable();
            $table->integer('created_by')->nullable();
            $table->string('notes')->nullable();
            $table->integer('approved')->nullable();
            $table->integer('pay_period_id')->unsigned()->nullable();
            $table->integer('bank_account_topup_id')->unsigned()->nullable();
            $table->integer("purchase_requisition_material_id")->unsigned()->nullable();
            $table->timestamps();

            $table->foreign('project_id')->references('id')->on('projects');
            $table->foreign('vendor_id')->references('id')->on('vendors');
            $table->foreign('chart_of_account_id')->references('id')->on('chart_of_accounts');
            $table->foreign('material_and_service_id')->references('id')->on('material_and_services');
            $table->foreign('bank_account_id')->references('id')->on('bank_accounts');
            $table->foreign('individual_project_phase_id')->references('id')->on('individual_project_phases');
            $table->foreign('vendor_contract_id')->references('id')->on('vendor_contracts');
            $table->foreign('pay_period_id')->references('id')->on('pay_periods');
            $table->foreign('bank_account_topup_id')->references('id')->on('bank_account_topups');
            $table->foreign('purchase_requisition_material_id')->references('id')->on('purchase_requisition_materials');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('expense_records');
    }
}
