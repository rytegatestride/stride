<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAccountFormsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('account_forms', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('client_id')->unsigned()->nullable();
            $table->integer('gender')->nullable();
            $table->integer('marital_status')->nullable();
            $table->string('next_of_kin')->nullable();
            $table->string('relationship')->nullable();
            $table->string('address')->nullable();
            $table->string('next_of_kin_address',500)->nullable();
            $table->string('next_of_kin_email')->nullable();
            $table->string('product_of_acount')->nullable();
            $table->text('bvn')->nullable();
            $table->string('signatory')->nullable();
            $table->string('six_digit_code')->nullable();
            $table->integer('employment_status')->nullable();
            $table->string('position')->nullable();
            $table->double('net_income')->nullable();
            $table->double('duration_of_employment')->nullable();
            $table->string('business_address',500)->nullable();
            $table->string('staff_id')->nullable();
            $table->timestamp('salary_date')->nullable();
            $table->string('guarantor_name')->nullable();
            $table->string('guarantor_phone')->nullable();
            $table->string('guarantor_address',300)->nullable();
            $table->string('guarantor_company',300)->nullable();
            $table->string('guarantor_email')->nullable();
            $table->string('guarantor_official_email')->nullable();
            $table->string('guarantor_occupation',300)->nullable();
            $table->double('guarantor_monthly_income')->nullable();
            $table->string('guarantor_company_address',300)->nullable();
            $table->string('account_name')->nullable();
            $table->string('account_number')->nullable();
            $table->integer('bank_id')->unsigned()->nullable();
            $table->string('alt_account_name')->nullable();
            $table->string('alt_account_number')->nullable();
            $table->integer('alt_bank_id')->unsigned()->nullable();
            $table->timestamps();
        });
       
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('account_forms');
    }
}
