<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddApprovedDateToVendorContracts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vendor_contracts',function($table){
            $table->timestamp('approved_date')->nullable();
        });
        Schema::table('purchase_orders',function($table){
            $table->timestamp('approved_date')->nullable();
        });
        Schema::table('purchase_requisition_materials',function($table){
            $table->timestamp('approved_date')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vendor_contracts',function($table){
            $table->dropColumn('approved_date');
        });
        Schema::table('purchase_orders',function($table){
            $table->dropColumn('approved_date');
        });
        Schema::table('purchase_requisition_materials',function($table){
            $table->dropColumn('approved_date');
        });
    }
}
