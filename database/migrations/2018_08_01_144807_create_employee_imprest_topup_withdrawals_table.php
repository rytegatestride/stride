<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeeImprestTopupWithdrawalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_imprest_topup_withdrawals', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('project_id')->unsigned();
            $table->integer('employee_imprest_topup_id')->unsigned()->nullable();
            $table->integer('project_phase_id')->unsigned();
            $table->integer('material_and_service_id')->unsigned()->nullable();
            $table->text('withdraw_date');
            $table->text('amount');
            $table->integer('employee_imprest_id')->unsigned()->nullable();
            $table->integer('asset_model_id')->unsigned()->nullable();
            $table->string('asset_type');
            $table->timestamps();

            $table->foreign('project_id')->references('id')->on('projects');
            $table->foreign('employee_imprest_topup_id', 'emp_imp_tup_id')->references('id')->on('employee_imprest_topups');
            $table->foreign('project_phase_id')->references('id')->on('project_phases');
            $table->foreign('material_and_service_id', 'emp_imp_tpup_wdrw_mat_serv_id')->references('id')->on('material_and_services');
            $table->foreign('employee_imprest_id')->references('id')->on('employee_imprests');
            $table->foreign('asset_model_id')->references('id')->on('asset_models');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_imprest_topup_withdrawals');
    }
}
