<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('material_and_service_id')->unsigned();
            $table->integer('project_id')->unsigned();
            $table->string('description',300);
            $table->double('price')->nullable();
            $table->integer('recurrence')->nullable();
            $table->double('set_up_fee')->nullable();

            $table->foreign('material_and_service_id')->references('id')->on('material_and_services');
            $table->foreign('project_id')->references('id')->on('projects');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
