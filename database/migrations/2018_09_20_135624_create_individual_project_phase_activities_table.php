<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIndividualProjectPhaseActivitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('individual_project_phase_activities', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('project_id')->unsigned();
            $table->integer('project_phase_activity_id')->unsigned();
            $table->string('duration')->nullable();
            $table->string('start_date')->nullable();
            $table->string('end_date')->nullable();
            $table->string('progress')->default(0);
            $table->timestamps();

            $table->foreign('project_id')->references('id')->on('projects');
            $table->foreign('project_phase_activity_id', 'ind_proj_ph_act_proj_phase_act_id')->references('id')->on('project_phase_activities');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('individual_project_phase_activities');
    }
}
