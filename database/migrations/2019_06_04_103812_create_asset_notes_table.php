<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssetNotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asset_notes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('liquid_asset_assignment_id')->unsigned();
            $table->string('note',500);
            $table->foreign('liquid_asset_assignment_id')->references('id')->on('liquid_asset_assignments');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('asset_notes');
    }
}
