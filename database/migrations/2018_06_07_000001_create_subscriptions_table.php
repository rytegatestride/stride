<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubscriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subscriptions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('role_id')->unsigned();
            $table->tinyInteger('new_budget')->default(0);
            $table->tinyInteger('budget_approval')->default(0);
            $table->tinyInteger('budget_build')->default(0);
            $table->tinyInteger('new_project')->default(0);
            $table->tinyInteger('car_pool')->default(0);
            $table->tinyInteger('project_phase')->default(0);
            $table->tinyInteger('new_contract')->default(0);
            $table->tinyInteger('contract_payment')->default(0);
            $table->tinyInteger('approve_contract')->default(0);
            $table->tinyInteger('new_purchase')->default(0);
            $table->tinyInteger('approve_purchase')->default(0);
            $table->tinyInteger('new_asset')->default(0);
            $table->tinyInteger('process_purchase')->default(0);
            $table->tinyInteger('review_budget')->default(0);
            $table->tinyInteger('paid_purchase')->default(0);
            $table->tinyInteger('delivered_purchase')->default(0);
            $table->tinyInteger('service_vendor')->default(0);
            $table->tinyInteger('review_project_costing')->default(0);
            $table->tinyInteger('approve_project_costing')->default(0);
            $table->tinyInteger('requisition_vendor_assignment')->default(0);
            $table->tinyInteger('requisition_updates')->default(0);
            $table->tinyInteger('new_project_costing')->default(0);
            $table->tinyInteger('new_requisition')->default(0);
            $table->tinyInteger('review_appraisal')->default(0);
            $table->timestamps();

            $table->foreign('role_id')->references('id')->on('roles');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subscriptions');
    }
}
