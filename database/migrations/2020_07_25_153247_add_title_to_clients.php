<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddTitleToClients extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('clients', function (Blueprint $table) {
            $table->string('job_title')->nullable();
            $table->string('title')->nullable();
            $table->string('website')->nullable();
            $table->string('social_media',500)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('clients', function (Blueprint $table) {
            $table->dropColumn('job_title')->nullable();
            $table->dropColumn('title')->nullable();
            $table->dropColumn('website')->nullable();
            $table->dropColumn('social_media',500)->nullable();
        });
    }
}
