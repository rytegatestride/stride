<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddLeadSourceToPermissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('permissions', function (Blueprint $table) {
            $table->tinyInteger('view_opportunities')->default('0');
            $table->tinyInteger('edit_opportunities')->default('0');
            $table->tinyInteger('lead_sources')->default('0');
            $table->tinyInteger('stages')->default('0');
            $table->tinyInteger('opportunity_types')->default('0');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('permissions', function (Blueprint $table) {
            $table->dropColumn('view_opportunities');
            $table->dropColumn('edit_opportunities');
            $table->dropColumn('lead_sources');
            $table->dropColumn('stages');
            $table->dropColumn('opportunity_types');
        });
    }
}
