<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddReceivedToInventoryTopups extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('inventory_topups', function (Blueprint $table) {
            $table->string('receiver_name')->nullable();
            $table->timestamp('delivery_date')->nullable();
            $table->integer('quantity_delivered')->nullable();
            $table->integer('inventory_location_id')->unsigned()->nullable();
            $table->string('delivered_by')->nullable();
            $table->integer('condition')->nullable();
            $table->text('note')->nullable();
            $table->integer('project_id')->unsigned()->nullable();
            $table->integer('individual_project_phase_id')->nullable();
            $table->text('material_description')->nullable();
            $table->string('image',300)->nullable();
            $table->integer('purchase_order_material_id')->unsigned()->nullable();
            $table->integer('purchase_requisition_material_id')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('inventory_topups', function (Blueprint $table) {
            $table->dropColumn('receiver_name');
            $table->dropColumn('delivery_date');
            $table->dropColumn('quantity_delivered');
            $table->dropColumn('inventory_location_id');
            $table->dropColumn('delivered_by');
            $table->dropColumn('condition');
            $table->dropColumn('note');
            $table->dropColumn('project_id');
            $table->dropColumn('individual_project_phase_id');
            $table->dropColumn('material_description');
            $table->dropColumn('image');
            $table->dropColumn('purchase_order_material_id');
            $table->dropColumn('purchase_requisition_material_id');
        });
    }
}
