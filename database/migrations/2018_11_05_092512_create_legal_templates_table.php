<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLegalTemplatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('legal_templates', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            //$table->integer('legal_template_category_id');
            $table->integer('material_and_services_id')->unsigned();
            $table->text('body');
            $table->foreign('material_and_services_id')->references('id')->on('material_and_services');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('legal_templates');
    }
}
