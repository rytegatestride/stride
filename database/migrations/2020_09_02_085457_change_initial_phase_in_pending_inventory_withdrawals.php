<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeInitialPhaseInPendingInventoryWithdrawals extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('pending_inventory_withdrawals', function (Blueprint $table) {
            $table->integer('initial_phase')->unsigned()->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('pending_inventory_withdrawals', function (Blueprint $table) {
            $table->dropColumn('initial_phase');
        });
    }
}
