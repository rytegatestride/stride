<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAppointmentLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appointment_locations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('appointment_id')->unsigned()->nullable();
            $table->bigInteger('lead_id')->unsigned()->nullable();
            $table->double('arrival_lat')->nullable();
            $table->double('arrival_lng')->nullable();
            $table->string('arrival_address',500)->nullable();
            $table->double('departure_lat')->nullable();
            $table->double('departure_lng')->nullable();
            $table->string('departure_address',500)->nullable();
            $table->integer('user_id')->unsigned();
            $table->timestamp('arrival_time')->nullable();
            $table->timestamp('departure_time')->nullable();
            $table->text('arrival_note')->nullable();
            $table->text('departure_note')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appointment_locations');
    }
}
