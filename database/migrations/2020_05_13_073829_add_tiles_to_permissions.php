<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTilesToPermissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('permissions',function($table){
            $table->tinyInteger('finance_administration_tile')->default('0');
            $table->tinyInteger('purchase_order_tile')->default('0');
            $table->tinyInteger('invoice_tile')->default('0');
        });
        Schema::table('users',function($table){
            $table->integer('last_payrole')->unsigned()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('permissions',function($table){
            $table->dropColumn('finance_administration_tile');
            $table->dropColumn('purchase_order_tile');
            $table->dropColumn('invoice_tile');
        });
    }
}
