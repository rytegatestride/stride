<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCostingDaysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('costing_days', function (Blueprint $table) {
            $table->increments('id');
            $table->string('date');
            $table->integer('costing_week_id')->unsigned();
            $table->integer('created_by');
            $table->timestamps();
            
            $table->foreign('costing_week_id')->references('id')->on('costing_weeks');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('costing_days');
    }
}
