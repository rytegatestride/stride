<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRevenueItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('revenue_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('material_and_services_category_id')->unsigned();
            $table->integer('service_type_id')->unsigned();
            $table->string('name');
            $table->integer('active')->default('1');
            $table->integer('chart_of_account_id')->unsigned()->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('revenue_items');
    }
}
