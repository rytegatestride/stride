<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddContractUpdatesToSubscriptions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('subscriptions',function($table){
            $table->tinyInteger('service_order_updates')->default('0');
            $table->tinyInteger('material_and_service_order_created')->default('0');
            $table->tinyInteger('material_and_service_order_updates')->default('0');
            $table->tinyInteger('material_and_service_order_payment')->default('0');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
