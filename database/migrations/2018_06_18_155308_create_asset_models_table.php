<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssetModelsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('asset_models', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('asset_manufacturer_id')->unsigned();
            $table->integer('asset_category_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->string('name');
            $table->tinyInteger('active');
            $table->timestamps();

            $table->foreign('asset_manufacturer_id')->references('id')->on('asset_manufacturers');
            $table->foreign('asset_category_id')->references('id')->on('asset_categories');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('asset_models');
    }
}
