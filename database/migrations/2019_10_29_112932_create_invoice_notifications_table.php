<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoiceNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoice_notifications', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('project_id')->unsigned();
            $table->integer('client_id')->unsigned();
            $table->integer('created_by')->unsigned();
            $table->timestamp('date')->nullable();
            $table->timestamp('due_date')->nullable();
            $table->integer('material_and_service_id')->unsigned()->nullable();
            $table->integer('qauntity');
            $table->double('price');
            $table->integer('asset_id')->unsigned()->nullable();
            $table->string('description',500)->nullable();
            $table->string('notes',500)->nullable();
            $table->integer('recurrence');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoice_notifications');
    }
}
