<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeAppraisals extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('appraisal_bases',function($table){
            $table->text('comment')->nullable()->change();
            $table->text('approve_comment')->nullable()->change();
            $table->text('decline_comment')->nullable()->change();
            $table->integer('appraisal_schedule_id')->unsigned()->nullable();
        });
        Schema::table('appraisals',function($table){
            $table->text('comment')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('appraisal_bases',function($table){
            $table->dropColumn('comment');
            $table->dropColumn('approve_comment');
            $table->dropColumn('decline_comment');
            $table->dropColumn('appraisal_schedule_id');
        });
        Schema::table('appraisals',function($table){
            $table->dropColumn('comment');
        });
    }
}
