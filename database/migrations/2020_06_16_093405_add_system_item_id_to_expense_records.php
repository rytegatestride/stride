<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSystemItemIdToExpenseRecords extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('expense_records',function($table){
            $table->integer('system_item_id')->unsigned()->nullable();
        });
        Schema::table('client_payments',function($table){
            $table->integer('system_item_id')->unsigned()->nullable();
        });
        Schema::table('bank_account_topups',function($table){
            $table->integer('system_item_id')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('expense_records',function($table){
            $table->dropColumn('system_item_id');
        });
        Schema::table('client_payments',function($table){
            $table->dropColumn('system_item_id');
        });
        Schema::table('bank_account_topups',function($table){
            $table->dropColumn('system_item_id');
        });
    }
}
