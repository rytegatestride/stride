<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddChangeToVendors extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vendors',function($table){
            $table->integer('country_id')->unsigned()->nullable()->change();
            $table->integer('state_id')->unsigned()->nullable()->change();
            $table->integer('city_id')->unsigned()->nullable()->change();
            $table->string('note',500)->nullable()->change();
            $table->integer('bank_id')->unsigned()->nullable()->change();
            $table->string('account_number')->nullable()->change();
            $table->string('account_name')->nullable()->change();
            $table->integer('rating')->nullable()->change();
            $table->string('address')->nullable()->change();
            $table->string('phone')->nullable()->change();
            $table->string('specialty')->nullable()->change();
            $table->integer('category_id')->unsigned()->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
