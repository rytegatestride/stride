<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddImprestToExpenseRecords extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('expense_records',function($table){
            $table->integer('employee_imprest_id')->unsigned()->nullable();
            $table->string('account_number')->nullable();
            $table->string('reciepient_name')->nullable();
            $table->string('reciepient_phone')->nullable();
            $table->string('reciepient_bank')->nullable();
            $table->integer('to')->nullable();
            $table->string('asset_type')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('expense_records',function($table){
            $table->dropColumn('employee_imprest_id');
            $table->dropColumn('account_number');
            $table->dropColumn('reciepient_name');
            $table->dropColumn('reciepient_phone');
            $table->dropColumn('reciepient_bank');
            $table->dropColumn('to');
            $table->dropColumn('asset_type');
        });
    }
}
