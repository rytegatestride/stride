<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectLaboursTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_labours', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('material_and_services_id')->unsigned();
            $table->integer('individual_project_phase_id')->unsigned();
            $table->integer('costing_day_id')->unsigned()->nullable();
            $table->integer('quantity');
            $table->string('amount')->default(0);
            $table->string('extra')->default(0);
            $table->string('start_date')->nullable();
            $table->string('end_date')->nullable();
            $table->string('progress')->nullable();
            $table->timestamps();

            $table->foreign('costing_day_id')->references('id')->on('costing_days');
            $table->foreign('material_and_services_id')->references('id')->on('material_and_services');
            $table->foreign('individual_project_phase_id')->references('id')->on('individual_project_phases');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_labours');
    }
}
