<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeAssetManufacturers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('asset_manufacturers',function($table){
            $table->integer('asset_category_id')->unsigned()->nullable()->change();
            $table->integer('user_id')->unsigned()->nullable()->change();
            $table->integer('active')->default('1')->change();
        });
        Schema::table('asset_models',function($table){
            $table->integer('asset_manufacturer_id')->unsigned()->nullable()->change();
            $table->integer('user_id')->unsigned()->nullable()->change();
            $table->integer('asset_category_id')->unsigned()->change();
            $table->integer('active')->default('1')->change();
        });
        Schema::table('asset_categories',function($table){
            $table->integer('active')->default('1')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
