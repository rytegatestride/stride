<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMaterialIdToVendorContracts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vendor_contracts',function($table){
            $table->integer('material_id')->unsigned()->nullable();
            $table->foreign('material_id')->references('id')->on('material_and_services');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vendor_contracts',function($table){
            $table->dropColumn('material_id');
        });
    }
}
