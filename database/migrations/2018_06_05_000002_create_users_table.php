<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('email')->unique();
            $table->string('password')->nullable();
            $table->integer('role_id')->unsigned()->nullable();
            $table->integer('department_id')->unsigned()->nullable();
            $table->integer('position_id')->unsigned()->nullable();
            $table->integer('office_location_id')->unsigned()->nullable();
            $table->integer('division_id')->unsigned()->nullable();
            $table->integer('management_level_id')->unsigned()->nullable();
            $table->string('phone_no')->nullable();
            $table->string('relative_name')->nullable();
            $table->string('relative_relationship')->nullable();
            $table->string('relative_phone_no')->nullable();
            $table->string('alternative_phone_no')->nullable();
            $table->integer('bank_id')->unsigned()->nullable();
            $table->string('gender');
            $table->string('account_no')->nullable();
            $table->integer('account_type')->nullable();
            $table->double('salary',8,2)->default(0);
            $table->string('pay_frequency')->nullable();
            $table->string('salary_component')->nullable();
            $table->tinyInteger('active')->nullable();
            $table->tinyInteger('paye')->default(0);
            $table->tinyInteger('employee_imprest')->default(0);
            $table->tinyInteger('confirmed')->default(0);

            $table->foreign('division_id')->references('id')->on('divisions');
            $table->foreign('role_id')->references('id')->on('roles');
            $table->foreign('department_id')->references('id')->on('departments');
            $table->foreign('position_id')->references('id')->on('user_positions');
            $table->foreign('office_location_id')->references('id')->on('office_locations');
            $table->foreign('bank_id')->references('id')->on('banks');
            $table->foreign('management_level_id')->references('id')->on('management_levels');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
