<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSalaryToArchives extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('archives',function($table){
            $table->double('net_pay')->nullable();
            $table->double('salary')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('archives',function($table){
            $table->dropColumn('basic');
            $table->dropColumn('housing');
            $table->dropColumn('transport');
            $table->dropColumn('medical');
            $table->dropColumn('pension');
            $table->dropColumn('meal');
            $table->dropColumn('utilities');
            $table->dropColumn('leave');
            $table->dropColumn('dressing');
            $table->dropColumn('entertainment');
            $table->dropColumn('net_pay');
            $table->dropColumn('paye');
            $table->dropColumn('salary');
        });
    }
}
