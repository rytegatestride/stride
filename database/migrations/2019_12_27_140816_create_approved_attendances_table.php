<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApprovedAttendancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('approved_attendances', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->timestamp('date')->nullable();
            $table->timestamp('punched_in')->nullable();
            $table->timestamp('punched_out')->nullable();
            $table->float('normal_duration')->nullable();
            $table->float('excess_duration')->nullable();
            $table->float('total_duration');
            $table->string('multiplier')->nullable();
            $table->integer('month_id');
            $table->integer('week_id');

            $table->float('vacation')->nullable();
            $table->string('leave_interval')->nullable();
            $table->string('leave_multiplier')->nullable();

            $table->string('excess_interval')->nullable();
            $table->string('normal_interval')->nullable();
            $table->integer('overtime')->nullable();

            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('approved_attendances');
    }
}
