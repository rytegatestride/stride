<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUtilityBillPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('utility_bill_payments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('utility_bill_id')->unsigned();
            $table->double('amount');
            $table->foreign('utility_bill_id')->references('id')->on('utility_bills');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('utility_bill_payments');
    }
}
