<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddInvoiceShippingToClientPayments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('client_payments',function($table){
            $table->integer('invoice_shipping')->nullable();
        });
        Schema::table('bank_account_topups',function($table){
            $table->integer('invoice_shipping')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('client_payments',function($table){
            $table->dropColumn('invoice_shipping')->nullable();
        });
        Schema::table('bank_account_topups',function($table){
            $table->dropColumn('invoice_shipping');
        });
    }
}
