<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaterialAndServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('material_and_services', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('material_and_services_category_id')->unsigned();
            $table->integer('service_type_id')->unsigned();
            $table->string('name');
            $table->tinyInteger('active');
            $table->timestamps();

            $table->foreign('material_and_services_category_id')->references('id')->on('material_and_service_categories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('material_and_services');
    }
}
