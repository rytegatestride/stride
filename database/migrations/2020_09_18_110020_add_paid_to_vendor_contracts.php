<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddPaidToVendorContracts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vendor_contracts', function (Blueprint $table) {
            $table->double('paid')->default('0');
            $table->double('balance')->default('0');
            $table->double('tax_paid')->default('0');
            $table->double('tax_balance')->default('0');
            $table->double('shipping_paid')->default('0');
            $table->double('shipping_balance')->default('0');
        });
        Schema::table('purchase_orders', function (Blueprint $table) {
            $table->double('paid')->default('0');
            $table->double('balance')->default('0');
            $table->double('tax_paid')->default('0');
            $table->double('tax_balance')->default('0');
            $table->double('shipping_paid')->default('0');
            $table->double('shipping_balance')->default('0');
        });
        Schema::table('purchase_requisition_materials', function (Blueprint $table) {
            $table->double('paid')->default('0');
            $table->double('balance')->default('0');
            $table->double('tax_paid')->default('0');
            $table->double('tax_balance')->default('0');
            $table->double('shipping_paid')->default('0');
            $table->double('shipping_balance')->default('0');
        });
        Schema::table('purchase_orders_materials', function (Blueprint $table) {
            $table->double('paid')->default('0');
            $table->double('balance')->default('0');
        });
        Schema::table('purchase_orders_services', function (Blueprint $table) {
            $table->double('paid')->default('0');
            $table->double('balance')->default('0');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('vendor_contracts', function (Blueprint $table) {
            $table->dropColumn('paid');
            $table->dropColumn('balance');
            $table->dropColumn('tax_paid');
            $table->dropColumn('tax_balance');
            $table->dropColumn('shipping_paid');
            $table->dropColumn('shipping_balance');
        });
        Schema::table('purchase_orders', function (Blueprint $table) {
            $table->dropColumn('paid');
            $table->dropColumn('balance');
            $table->dropColumn('tax_paid');
            $table->dropColumn('tax_balance');
            $table->dropColumn('shipping_paid');
            $table->dropColumn('shipping_balance');
        });
        Schema::table('purchase_requisition_materials', function (Blueprint $table) {
            $table->dropColumn('paid');
            $table->dropColumn('balance');
            $table->dropColumn('tax_paid');
            $table->dropColumn('tax_balance');
            $table->dropColumn('shipping_paid');
            $table->dropColumn('shipping_balance');
        });
        Schema::table('purchase_orders_materials', function (Blueprint $table) {
            $table->dropColumn('paid');
            $table->dropColumn('balance');
        });
        Schema::table('purchase_orders_services', function (Blueprint $table) {
            $table->dropColumn('paid');
            $table->dropColumn('balance');
        });
    }
}
