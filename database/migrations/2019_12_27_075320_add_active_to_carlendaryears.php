<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddActiveToCarlendaryears extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('carlendaryears',function($table){
            $table->integer('active')->default('0');
        });
        Schema::table('leave_entitlements',function($table){
            $table->integer('carlendaryear_id')->unsigned()->nullable();
            $table->foreign('carlendaryear_id')->references('id')->on('carlendaryears');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('carlendaryears',function($table){
            $table->dropColumn('active');
        });
        Schema::table('leave_entitlements',function($table){
            $table->dropColumn('carlendaryear_id');
        });
    }
}
