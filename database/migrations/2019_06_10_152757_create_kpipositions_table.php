<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKpipositionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kpipositions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_position_id')->unsigned();
            $table->integer('kpi_id')->unsigned();
            $table->foreign('user_position_id')->references('id')->on('user_positions');
            $table->foreign('kpi_id')->references('id')->on('kpis');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kpipositions');
    }
}
