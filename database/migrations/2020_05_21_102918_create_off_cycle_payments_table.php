<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOffCyclePaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('off_cycle_payments', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamp('date')->nullable();
            $table->string('name');
            $table->integer('pay_period_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->double('amount');
            $table->integer('project_id')->unsigned();
            $table->integer('individual_project_phase_id')->unsigned();
            $table->integer('bank_account_id')->unsigned();

            $table->foreign('pay_period_id')->references('id')->on('pay_periods');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('project_id')->references('id')->on('projects');
            $table->foreign('individual_project_phase_id')->references('id')->on('individual_project_phases');
            $table->foreign('bank_account_id')->references('id')->on('bank_accounts');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('off_cycle_payments');
    }
}
