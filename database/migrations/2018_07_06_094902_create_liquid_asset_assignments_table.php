<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLiquidAssetAssignmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('liquid_asset_assignments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('liquid_asset_id')->unsigned();
            $table->integer('assigned_to')->nullable();
            $table->string('user_type')->nullable();
            $table->tinyInteger('assignment_status')->nullable();
            $table->text('assignment_start')->nullable();
            $table->text('assignment_end')->nullable();
            $table->text('actual_return_date')->nullable();
            $table->text('assignment_note')->nullable();
            $table->integer('return_status')->default('0');
            $table->string('return_note')->nullable();
            $table->timestamps();

            $table->foreign('liquid_asset_id')->references('id')->on('liquid_assets');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('liquid_asset_assignments');
    }
}
