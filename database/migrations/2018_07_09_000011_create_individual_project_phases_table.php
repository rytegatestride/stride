<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIndividualProjectPhasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('individual_project_phases', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('project_id')->unsigned();
            $table->integer('project_phase_id')->unsigned();
            $table->string('cost');
            $table->string('start_date');
            $table->string('end_date');
            $table->integer('status')->default(0);
            $table->integer('approved_by')->default(0);
            $table->integer('declined_by')->default(0);
            $table->integer('canceled_by')->default(0);
            $table->integer('review_submitter')->default(0);
            $table->timestamps();

            $table->foreign('project_id')->references('id')->on('projects');
            $table->foreign('project_phase_id')->references('id')->on('project_phases');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('individual_project_phases');
    }
}
