<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddOffCyclePaymentIdToBankAccountTopups extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bank_account_topups', function (Blueprint $table) {
            $table->integer('off_cycle_payment_id')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bank_account_topups', function (Blueprint $table) {
            $table->dropColumn('off_cycle_payment_id');
        });
    }
}
