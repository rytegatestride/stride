<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaterialTaxesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('material_taxes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('purchase_requisition_material_id')->unsigned();
            $table->double('component');
            $table->double('amount');
            $table->integer('percentage')->default('1');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('material_taxes');
    }
}
