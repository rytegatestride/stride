<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDestinationToEmployeeImprestTopupWithdrawals extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('employee_imprest_topup_withdrawals',function($table){
            $table->integer('to')->nullable();
            $table->integer('pr_material_id')->unsigned()->nullable();
            $table->integer('contract_id')->unsigned()->nullable();
            $table->foreign('pr_material_id')->references('id')->on('purchase_requisition_materials');
            $table->foreign('contract_id')->references('id')->on('vendor_contracts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('employee_imprest_topup_withdrawals',function($table){
            $table->dropColumn('to');
            $table->dropColumn('pr_material_id');
            $table->dropColumn('contract_id');
        });
    }
}
