<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTaxToBankAccountTopups extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('bank_account_topups',function($table){
            $table->integer('po_tax')->nullable();
            $table->integer('po_shipping')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('bank_account_topups',function($table){
            $table->dropColumn('po_tax');
            $table->dropColumn('po_shipping');
        });
    }
}
