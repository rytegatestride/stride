<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLatOutToAttendnaces extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('attendances',function($table){
            $table->double('lat_out')->nullable();
            $table->double('lng_out')->nullable();
            $table->string('address_out',300)->nullable();
            $table->string('ip')->nullable();
            $table->string('ip_out')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('attendances',function($table){
            $table->dropColumn('lat_out');
            $table->dropColumn('lng_out');
            $table->dropColumn('address_out');
            $table->dropColumn('ip');
            $table->dropColumn('ip_out');
        });
    }
}
