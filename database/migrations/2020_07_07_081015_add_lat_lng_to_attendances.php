<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddLatLngToAttendances extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('attendances',function($table){
            $table->double('lat')->nullable();
            $table->double('lng')->nullable();
            $table->string('address',300)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('attendances',function($table){
            $table->dropColumn('lat');
            $table->dropColumn('lng');
            $table->dropColumn('address');
        });
    }
}
