<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePurchaseRequestMaterialsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_request_materials', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('purchase_request_id')->unsigned();
            $table->integer('material_and_service_id')->unsigned();
            $table->double('price')->default('0');
            $table->integer('quantity');
            $table->text('description')->nullable();
            $table->integer('vendor_id')->nullable();
            $table->integer('liquid_asset_id')->unsigned()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('purchase_request_materials');
    }
}
