<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAssetToPurchaseOrderMaterials extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('purchase_orders_materials',function($table){
            $table->integer('is_asset')->nullable();
            $table->integer('liquid_asset_id')->unsigned()->nullable();
            $table->foreign('liquid_asset_id')->references('id')->on('liquid_assets');
        });
        Schema::table('purchase_orders_services',function($table){
            $table->integer('is_asset')->nullable();
            $table->integer('liquid_asset_id')->unsigned()->nullable();
            $table->foreign('liquid_asset_id')->references('id')->on('liquid_assets');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('purchase_orders_materials',function($table){
            $table->dropColumn('is_asset');
            $table->dropColumn('liquid_asset_id');
        });
        Schema::table('purchase_orders_services',function($table){
            $table->dropColumn('is_asset');
            $table->dropColumn('liquid_asset_id');
        });
    }
}
