<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBenefitDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('benefit_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('benefit_id')->unsigned();
            $table->integer('type')->default('0');
            $table->integer('benefit_type_id')->unsigned();
            $table->integer('start_payperiod')->unsigned();
            $table->integer('end_payperiod')->unsigned()->nullable();
            $table->integer('no_end')->nullable();
            $table->double('amount');
            $table->integer('percentage')->default('0');

            $table->foreign('benefit_id')->references('id')->on('benefits');
            $table->foreign('benefit_type_id')->references('id')->on('benefit_types');
            $table->foreign('start_payperiod')->references('id')->on('pay_periods');
            $table->foreign('end_payperiod')->references('id')->on('pay_periods');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('benefit_details');
    }
}
