<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIncidentToPermissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('permissions',function($table){
            $table->tinyInteger('view_incidents')->default('0');
            $table->tinyInteger('edit_incidents')->default('0');
            $table->tinyInteger('view_incident_types')->default('0');
            $table->tinyInteger('edit_incident_types')->default('0');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('permissions',function($table){
            $table->dropColumn('view_incidents');
            $table->dropColumn('edit_incidents');
            $table->dropColumn('view_incident_types');
            $table->dropColumn('edit_incident_types');
        });
    }
}
