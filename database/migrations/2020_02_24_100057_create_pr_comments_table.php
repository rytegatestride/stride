<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePrCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pr_comments', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('initial_status')->nullable();
            $table->integer('new_status')->nullable();
            $table->string('comment',500);
            $table->integer('user_id')->unsigned();
            $table->integer('pr_id')->unsigned();
            $table->integer('pr_material_id')->unsigned()->nullable();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('pr_id')->references('id')->on('purchase_requisitions');
            $table->foreign('pr_material_id')->references('id')->on('purchase_requisition_materials');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pr_comments');
    }
}
