<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAssetIdToInvoiceDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('invoice_details',function($table){
            $table->integer('asset_id')->unsigned()->nullable();
            $table->string('description')->nullable();
            $table->foreign('asset_id')->references('id')->on('liquid_assets');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('invoice_details',function($table){
            $table->dropColumn('asset_id');
            $table->dropColumn('description');
        });
    }
}
