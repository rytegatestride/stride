<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPendingToPermissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('permissions',function($table){
            $table->tinyInteger('pending_withdrawals')->default('0');
            $table->tinyInteger('approve_pending_withdrawals')->default('0');
            $table->tinyInteger('approve_imprest_requests')->default('0');
            $table->tinyInteger('view_imprest_requests')->default('0');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('permissions',function($table){
            $table->dropColumn('pending_withdrawals');
            $table->dropColumn('approve_pending_withdrawals');
            $table->dropColumn('approve_imprest_requests');
            $table->dropColumn('view_imprest_requests');
        });
    }
}
