<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProjectIdToUtilityBills extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('utility_bills',function($table){
            $table->integer('project_id')->unsigned()->nullable();
            $table->integer('individual_project_phase_id')->unsigned()->nullable();
            $table->foreign('project_id')->references('id')->on('projects');
            $table->foreign('individual_project_phase_id')->references('id')->on('individual_project_phases');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('utility_bills',function($table){
            $table->dropColumn('project_id');
            $table->dropColumn('individual_project_phase_id');
        });
    }
}
