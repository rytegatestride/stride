<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateIndividualgoalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('individualgoals', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('departmentalgoal_id')->unsigned();
            $table->string('name');
            $table->foreign('departmentalgoal_id')->references('id')->on('departmentalgoals');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('individualgoals');
    }
}
