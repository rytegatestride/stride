<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUtilityBillSettingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('utility_bill_settings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('daily')->default('1');
            $table->integer('weekly')->default('1');
            $table->integer('monthly')->default('1');
            $table->integer('annually')->default('1');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('utility_bill_settings');
    }
}
