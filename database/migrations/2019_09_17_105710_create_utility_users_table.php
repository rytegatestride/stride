<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUtilityUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('utility_users', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('utility_bill_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->foreign('utility_bill_id')->references('id')->on('utility_bills');
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('utility_users');
    }
}
