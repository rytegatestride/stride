<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIncomeToPermissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('permissions',function($table){
            $table->tinyInteger('income_by_date_range')->default('0');
            $table->tinyInteger('income_by_division')->default('0');
            $table->tinyInteger('expense_by_division')->default('0');
            $table->tinyInteger('income_by_project')->default('0');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('permissions',function($table){
            $table->dropColumn('income_by_date_range');
            $table->dropColumn('income_by_division');
            $table->dropColumn('expense_by_division');
            $table->dropColumn('income_by_project');
        });
    }
}
