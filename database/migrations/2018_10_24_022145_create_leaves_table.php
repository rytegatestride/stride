<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeavesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('leaves', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('start_date');
            $table->string('end_date');
            $table->string('notes');
            $table->integer('leave_type_id')->unsigned();
            $table->string('start_time');
            $table->string('end_time');
            $table->integer('status')->default('0');
            $table->integer('approved_by')->nullable();
            $table->integer('declined_by')->nullable();
            $table->integer('assigned_by')->unsigned()->nullable();
            $table->timestamps();

            $table->foreign('assigned_by')->references('id')->on('users');
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('leave_type_id')->references('id')->on('leave_types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('leaves');
    }
}
