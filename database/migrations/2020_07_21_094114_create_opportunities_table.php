<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOpportunitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('opportunities', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id')->unsigned()->nullable();
            $table->string('title')->nullable();
            $table->integer('client_id')->unsigned();
            $table->timestamp('close_date')->nullable();
            $table->float('probability')->nullable();
            $table->bigInteger('opportunity_type_id')->nullable();
            $table->bigInteger('lead_source_id')->unsigned()->nullable();
            $table->bigInteger('stage_id')->unsigned();
            $table->bigInteger('campaign_source_id')->unsigned()->nullable();
            $table->double('amount')->nullable();
            $table->text('description')->nullable();
            $table->integer('status')->default('1');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('opportunities');
    }
}
