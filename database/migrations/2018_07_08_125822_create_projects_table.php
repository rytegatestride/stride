<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projects', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->text('project_date')->nullable();
            $table->integer('division_id')->unsigned();
            $table->integer('department_id')->unsigned();
            $table->integer('created_by')->nullable();
            $table->integer('coordinator')->nullable();
            $table->text('cost')->nullable();
            $table->smallInteger('project_costing')->default(0);
            $table->smallInteger('vendor_contract')->default(0);
            $table->integer('type')->default('0');
            $table->integer('purchase_requisition')->nullable();
            $table->smallInteger('active')->default(1);
            $table->foreign('division_id')->references('id')->on('divisions');
            $table->foreign('department_id')->references('id')->on('departments');
            $table->timestamp('start_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projects');
    }
}
