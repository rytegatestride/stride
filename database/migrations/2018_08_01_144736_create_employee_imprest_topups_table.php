<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeeImprestTopupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_imprest_topups', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('employee_imprest_id')->unsigned();
            $table->integer('topup_by');
            $table->text('topup_date');
            $table->integer('bank_id')->unsigned();
            $table->text('amount');
            $table->timestamps();

            $table->foreign('employee_imprest_id')->references('id')->on('employee_imprests');
            $table->foreign('bank_id')->references('id')->on('bank_accounts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_imprest_topups');
    }
}
