<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMaterialAndServiceIdToPurchaseOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('purchase_orders',function($table){
            $table->integer('material_and_service_id')->unsigned()->nullable();
            $table->integer('liquid_asset_id')->unsigned()->nullable();
            $table->foreign('material_and_service_id')->references('id')->on('material_and_services');
            $table->foreign('liquid_asset_id')->references('id')->on('liquid_assets');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('purchase_orders',function($table){
            $table->dropColumn('material_and_service_id');
            $table->dropColumn('liquid_asset_asset_id');
        });
    }
}
