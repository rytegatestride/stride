<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddChartOfAccountIdToEarnings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('earns',function($table){
            $table->integer('chart_of_account_id')->nullable();
        });
        Schema::table('deducts',function($table){
            $table->integer('chart_of_account_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('earns',function($table){
            $table->dropColumn('chart_of_account_id');
        });
        Schema::table('deducts',function($table){
            $table->dropColumn('chart_of_account_id');
        });
    }
}
