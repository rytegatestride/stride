<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePendingImprestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pending_imprests', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('imprest_id')->unsigned();
            $table->double('amount');
            $table->timestamp('date')->nullable();
            $table->integer('material_and_service_id')->unsigned();
            $table->integer('project_id')->unsigned();
            $table->integer('individual_project_phase_id')->unsigned();
            $table->string('recipient')->nullable();
            $table->string('note')->nullable();
            $table->integer('status')->default('0');

            $table->foreign('imprest_id')->references('id')->on('employee_imprests');
            $table->foreign('material_and_service_id')->references('id')->on('material_and_services');
            $table->foreign('project_id')->references('id')->on('projects');
            $table->foreign('individual_project_phase_id')->references('id')->on('individual_project_phases');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pending_imprests');
    }
}
