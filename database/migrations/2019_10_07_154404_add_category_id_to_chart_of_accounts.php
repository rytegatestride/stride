<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCategoryIdToChartOfAccounts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('chart_of_accounts',function($table){
            $table->integer('chart_of_account_category_id')->unsigned()->nullable();
            $table->foreign('chart_of_account_category_id')->references('id')->on('chart_of_account_categories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('chart_of_accounts',function($table){
            $table->dropColumn('chart_of_account_category_id');
        });
    }
}
