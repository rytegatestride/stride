<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAddLocationPurchaseOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('purchase_orders',function($table){
            $table->integer('inventory_location_id')->unsigned()->nullable();
            $table->string('address',500)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('purchase_orders',function($table){
            $table->dropColumn('inventory_location_id');
            $table->dropColumn('address');
        });
    }
}
