<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPeriodToAppraisalBases extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('appraisal_bases', function($table){
            $table->timestamp('period_start')->nullable();
            $table->timestamp('period_end')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('appraisal_bases', function($table){
            $table->dropColumn('period_start');
            $table->dropColumn('period_end');
        });
    }
}
