<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBankCodeToBanks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('banks',function($table){
            $table->string('routing_bank_code')->nullable();
        });
        Schema::table('users',function($table){
            $table->string('beneficiary_code')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('banks',function($table){
            $table->dropColumn('routing_bank_code');
        });
        Schema::table('users',function($table){
            $table->dropColumn('beneficiary_code');
        });
    }
}
