<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehicleRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicle_requests', function (Blueprint $table) {
            $table->increments('id');
            $table->text('departure_date');
            $table->text('return_date');
            $table->text('destination_address');
            $table->text('pickup_address');
            $table->text('created_by');
            $table->integer('request_status')->default(0);
            $table->integer('asset_id')->nullable();
            $table->text('notes');
            $table->string('approve_note')->nullable();
            $table->integer('country_id')->unsigned();
            $table->integer('state_id')->unsigned();
            $table->integer('city_id')->unsigned();
            $table->integer('dest_country_id')->unsigned();
            $table->integer('dest_state_id')->unsigned();
            $table->integer('dest_city_id')->unsigned();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicle_requests');
    }
}
