<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBenefitsToPermissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('permissions',function($table){
            $table->tinyInteger('view_benefit_types')->default('0');
            $table->tinyInteger('edit_benefit_types')->default('0');
            $table->tinyInteger('view_benefits')->default('0');
            $table->tinyInteger('edit_benefits')->default('0');
            $table->tinyInteger('view_earning_and_deduction')->default('0');
            $table->tinyInteger('edit_earning_and_deduction')->default('0');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
