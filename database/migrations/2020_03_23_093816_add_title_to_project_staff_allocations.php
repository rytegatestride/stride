<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTitleToProjectStaffAllocations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('project_staff_allocations',function($table){
            $table->string('title')->nullable();
            $table->string('responsibilities')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('project_staff_allocations',function($table){
            $table->dropColumn('title');
            $table->dropColumn('responsibilities');
        });
    }
}
