<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInventoryTopupWithdrawalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inventory_topup_withdrawals', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('inventory_topup_id')->unsigned();
            $table->integer('quantity');
            $table->timestamps();

            $table->foreign('inventory_topup_id')->references('id')->on('inventory_topups');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inventory_topup_withdrawals');
    }
}
