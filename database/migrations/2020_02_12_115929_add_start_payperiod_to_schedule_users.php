<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStartPayperiodToScheduleUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('schedule_users',function($table){
            $table->integer('pay_period_id')->unsigned()->nullable();
            $table->foreign('pay_period_id')->references('id')->on('pay_periods');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('schedule_users',function($table){
            $table->dropColumn('pay_period_id');
        });
    }
}
