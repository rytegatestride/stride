<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEarningAndDeductionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('earning_and_deductions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('type');
            $table->double('amount');
            $table->integer('pay_period_id')->unsigned();
            $table->integer('earn_id')->unsigned()->nullable();
            $table->integer('deduct_id')->unsigned()->nullable();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('pay_period_id')->references('id')->on('pay_periods');
            $table->foreign('earn_id')->references('id')->on('earns');
            $table->foreign('deduct_id')->references('id')->on('deducts');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('earning_and_deductions');
    }
}
