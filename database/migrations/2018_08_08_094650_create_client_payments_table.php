<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClientPaymentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('client_payments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('amount');
            $table->string('date');
            $table->string('note');
            $table->integer('status')->nullable();
            $table->integer('project_id')->unsigned();
            $table->integer('invoice_id')->unsigned();
            $table->integer('payment_category_id')->unsigned()->nullable();
            $table->integer('bank_account_id')->unsigned();
            $table->integer('receiver');
            $table->integer('created_by');
            $table->string('payment_method');
            $table->timestamps();

            $table->foreign('project_id')->references('id')->on('projects');
            $table->foreign('invoice_id')->references('id')->on('invoices');
            $table->foreign('payment_category_id')->references('id')->on('payment_categories');
            $table->foreign('bank_account_id')->references('id')->on('bank_accounts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('client_payments');
    }
}
