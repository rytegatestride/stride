<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStatusToAppraisalDates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('appraisal_dates', function($table){
            $table->integer('status')->default('0');
        });
        Schema::table('custom_appraisal_dates', function($table){
            $table->integer('status')->default('0');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('appraisal_dates', function($table){
            $table->dropColumn('status');
        });
        Schema::table('custom_appraisal_dates', function($table){
            $table->dropColumn('status')->default('0');
        });
    }
}
