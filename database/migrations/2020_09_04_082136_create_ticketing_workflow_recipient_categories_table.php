<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTicketingWorkflowRecipientCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ticketing_workflow_recipient_categories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('ticketing_workflow_recipient')->unsigned();
            $table->integer('asset_category_id')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ticketing_workflow_recipient_categories');
    }
}
