<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddApprovalLogIdToLeadAudits extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('lead_audits', function (Blueprint $table) {
            $table->dropColumn('old_value');
            $table->dropColumn('new_value');
            $table->bigInteger('approval_log_id')->unsigned()->nullable();
            $table->longText('details_snapshot')->nullable();
            $table->longText('opp_snapshot')->nullable();
            $table->longText('account_snapshot')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('lead_audits', function (Blueprint $table) {
            $table->dropColumn('approval_log_id');
        });
    }
}
