<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSchedulingToPermissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('permissions',function($table){
            $table->tinyInteger('view_schedules')->default('0');
            $table->tinyInteger('edit_schedules')->default('0');
            $table->tinyInteger('approve_timesheet')->default('0');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('permissions',function($table){
            $table->dropColumn('view_schedules');
            $table->dropColumn('edit_schedules');
            $table->dropColumn('approve_timesheet');
        });
    }
}
