<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePayConfigurationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pay_configurations', function (Blueprint $table) {
            $table->increments('id');
            $table->float('basic')->default('0');
            $table->float('housing')->default('0');
            $table->float('transport')->default('0');
            $table->float('medical')->default('0');
            $table->float('pension')->default('0');
            $table->float('meal')->default('0');
            $table->float('utilities')->default('0');
            $table->float('leave')->default('0');
            $table->float('dressing')->default('0');
            $table->float('entertainment')->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pay_configurations');
    }
}
