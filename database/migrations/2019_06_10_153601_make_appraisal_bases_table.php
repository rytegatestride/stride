<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakeAppraisalBasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appraisal_bases', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('appraised_by')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->timestamp('appraisal_year');
            $table->integer('qaurter')->nullable();
            $table->integer('status')->default('0');
            $table->string('comment',800)->nullable();
            $table->integer('reviewer')->unsigned()->nullable();
            $table->string('approve_comment',800)->nullable();
            $table->string('decline_comment',800)->nullable();

            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('appraised_by')->references('id')->on('users');
            $table->foreign('reviewer')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appraisal_bases');
    }
}
