<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLeadCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lead_comments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('initial_status')->nullable();
            $table->integer('new_status')->nullable();
            $table->text('comment')->nullable();
            $table->integer('user_id')->unsinged()->nullable();
            $table->integer('sales_lead_id')->unsinged()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lead_comments');
    }
}
