<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRecurrenceFeeToClientProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('client_products',function($table){
            $table->timestamp('reg_date')->nullable();
            $table->double('fee')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('client_products',function($table){
            $table->dropColumn('reg_date');
            $table->dropColumn('fee');
        });
    }
}
