<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjectPhaseActivitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('project_phase_activities', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('project_phase_id')->unsigned();
            $table->integer('material_and_service_id')->unsigned();       
            $table->timestamps();

            $table->foreign('project_phase_id')->references('id')->on('project_phases');
            $table->foreign('material_and_service_id')->references('id')->on('material_and_services');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('project_phase_activities');
    }
}
