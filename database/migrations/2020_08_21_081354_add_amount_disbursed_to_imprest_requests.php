<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddAmountDisbursedToImprestRequests extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('imprest_requests', function (Blueprint $table) {
            $table->double('amount_disbursed')->default('0');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('imprest_requests', function (Blueprint $table) {
            $table->dropColumn('amount_disbursed');
        });
    }
}
