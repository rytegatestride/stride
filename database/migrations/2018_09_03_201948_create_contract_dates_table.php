<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContractDatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contract_dates', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('vendor_contract_id')->unsigned();
            $table->string('amount');
            $table->string('date');
            $table->timestamps();

            $table->foreign('vendor_contract_id')->references('id')->on('vendor_contracts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contract_dates');
    }
}
