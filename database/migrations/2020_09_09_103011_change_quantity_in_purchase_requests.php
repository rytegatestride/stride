<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ChangeQuantityInPurchaseRequests extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('purchase_request_materials', function (Blueprint $table) {
            $table->float('quantity')->change();
        });
        Schema::table('purchase_request_services', function (Blueprint $table) {
            $table->float('quantity')->change();
        });
        Schema::table('purchase_orders_materials', function (Blueprint $table) {
            $table->float('quantity')->change();
        });
        Schema::table('purchase_orders_services', function (Blueprint $table) {
            $table->float('quantity')->change();
        });
        Schema::table('purchase_request_materials', function (Blueprint $table) {
            $table->float('quantity')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('purchase_requests', function (Blueprint $table) {
            //
        });
    }
}
