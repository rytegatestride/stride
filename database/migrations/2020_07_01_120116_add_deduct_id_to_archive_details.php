<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDeductIdToArchiveDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('archive_columns',function($table){
            $table->integer('benefit_type_id')->unsigned()->nullable();
        });
        Schema::table('archive_details',function($table){
            $table->integer('earn_id')->unsigned()->nullable();
            $table->integer('deduct_id')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('archive_columns',function($table){
            $table->dropColumn('benefit_type_id');
        });
        Schema::table('archive_details',function($table){
            $table->dropColumn('earn_id');
            $table->dropColumn('deduct_id');
        });
    }
}
