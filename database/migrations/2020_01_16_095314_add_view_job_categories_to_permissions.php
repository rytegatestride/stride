<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddViewJobCategoriesToPermissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        schema::table('permissions',function($table){
            $table->tinyInteger('view_job_categories')->default('0');
            $table->tinyInteger('edit_job_categories')->default('0');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        schema::table('permissions',function($table){
            $table->dropColumn('view_job_categories');
            $table->dropColumn('edit_job_categories');
        });
    }
}
