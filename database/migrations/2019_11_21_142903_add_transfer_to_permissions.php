<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTransferToPermissions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('permissions',function($table){
            $table->tinyInteger('transfer_funds')->default('0');
            $table->tinyInteger('recieve_funds')->default('0');
            $table->tinyInteger('all_bank_accounts')->default('0');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('permissions',function($table){
            $table->dropColumn('transfer_funds');
            $table->dropColumn('recieve_funds');
            $table->dropColumn('all_bank_accounts');
        });
    }
}
