<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateApprovedAttendanceItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('approved_attendance_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('approved_attendance_id');
            $table->string('in')->nullable();
            $table->string('out')->nullable();
            $table->string('comment');
            $table->double('duration');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('approved_attendance_items');
    }
}
