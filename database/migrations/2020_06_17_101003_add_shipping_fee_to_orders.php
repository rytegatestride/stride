<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddShippingFeeToOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders',function($table){
            $table->double('shipping_fee')->nullable();
        });
        Schema::table('invoices',function($table){
            $table->double('shipping_fee')->nullable();
        });
       
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders',function($table){
            $table->dropColumn('shipping_fee');
        });
        Schema::table('invoices',function($table){
            $table->dropColumn('shipping_fee');
        });
    }
}
