<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Tests\Traits\AttachJwtToken;

class PurchaseRequest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    use WithFaker,RefreshDatabase,AttachJwtToken;
    public function testItValidatesData()
    {
        $response = $this->post('/purchase_request');
        dump($response);

        $response->assertStatus(422)
        ->assertJson([
            'error'=>'vendor id is required'
        ]);
    }
}
