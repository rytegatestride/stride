<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Tests\Traits\AttachJwtToken;

class SalesLeadTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    use WithFaker,RefreshDatabase,AttachJwtToken;
    public function testBasicTest()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
    }
    // public function testItcanCreateALeadWithOnlyFirstName()
    // {
    //     $this->withoutExceptionHandling();
    //     $data=[
    //         'first_name'=>$this->faker->word
    //     ];
    //     $response = $this->post('/api/sales_leads',$data);

    //     $this->assertDatabaseHas('clients',['firstname'=>$data['first_name']]);
    //     $response->assertStatus(200);
    // }

    // public function testItcanCreateALeadWithOnlyLastName()
    // {
    //     $this->withoutExceptionHandling();
    //     $data=[
    //         'last_name'=>$this->faker->word
    //     ];
    //     $response = $this->post('/api/sales_leads',$data);

    //     $this->assertDatabaseHas('clients',['lastname'=>$data['last_name']]);
    //     $response->assertStatus(200);
    // }

    // public function testItcanCreateALeadWithOnlyCompany()
    // {
    //     $this->withoutExceptionHandling();
    //     $data=[
    //         'company'=>$this->faker->sentence
    //     ];
    //     $response = $this->post('/api/sales_leads',$data);

    //     $this->assertDatabaseHas('clients',$data);
    //     $response->assertStatus(200);
    // }

    // public function testItCannotCreateALeadWithoutFirstNameOrLastNameOrCompany()
    // {
    //     $this->withoutExceptionHandling();
    //     $data=[
            
    //     ];
    //     $response = $this->post('/api/sales_leads',$data);

    //     $response->assertStatus(400)
    //     ->assertJson([
    //         'error' => 'Either First Name, Last Name or Company is required',
    //     ]);
    // }

    // public function test_it_Can_Get_A_Lead_With_ID()
    // {
    //     $this->withoutExceptionHandling();
       
    //     $data=[
    //         'company'=>'my company'
    //     ];
    //     $response2 = $this->post('/api/sales_leads',$data);

    //     $response = $this->get('/api/get/lead/1');

    //     $response->assertStatus(200)
    //     ->assertJson([
    //         'company'=>'my company',
    //         'is_lead'=>1
    //     ]);
    // }
}
