<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Tests\Traits\AttachJwtToken;
use App\Project;
use App\Client;
use App\User;
use App\Invoice;
use App\InvoiceDetail;

class InvoiceStatusTest extends TestCase
{
    use WithFaker,RefreshDatabase,AttachJwtToken;
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_it_sets_status_to_unpaid_when_a_new_invoice_is_created()
    {
        $response = $this->get('/');

        $response->assertStatus(200);
        // $client=factory(Client::class)->create();
        // $project=factory(Project::class)->create();
        // $user=factory(User::class)->create();
        // $data=[
        //     'client'=>$client->id,
        //     'project'=>$project->id,
        //     'owner'=>$user->id,
        //     'start_date'=>date('Y-m-d'),
        //     'due_date'=>date('Y-m-d')
        // ];
        // $response = $this->post('/api/add/invoice',$data);

        // $response->assertStatus(200)
        // ->assertJson([
        //     'invoice_status_id'=>null,
        //     'payment_status'=>"0"
        // ]);
    }

    // public function test_it_can_changes_payment_status_when_payment_is_made()
    // {
    //     $client=factory(Client::class)->create();
    //     $project=factory(Project::class)->create();
    //     $user=factory(User::class)->create();
    //     $invoice=factory(Invoice::class)->create();
    //     $invoice_detail=new InvoiceDetail();
    //     $invoice_detail->invoice_id=$invoice->id;
    //     $invoice_detail->product_id=1;
    //     $invoice_detail->price=5000;
    //     $invoice_detail->quantity=1;
    //     $invoice_detail->description="something";
    //     $invoice_detail->save();

        
    // }
}
