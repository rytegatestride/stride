<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        
        <title>Stride ERP</title>

        <link rel="stylesheet" rel="icon" href="{{asset('images/rytegate-fav.png')}}" type="image/x-icon" />
        <link rel="stylesheet" type="text/css" href="{{asset(mix('css/all.css'),true)}}">
        <link rel="stylesheet" type="text/css" href="{{asset('fonts/font-awesome/css/font-awesome.min.css')}}">
        <style type="text/css">
            [v-cloak] {
              display:none
            }

            .spinner:before{
              content: "";
              box-sizing: border-box;
              position: absolute;
              top: 40%;
              left: 45%;
              height: 75px;
              width: 75px;
              margin-top: 24px;
              margin-left: 36px;
              border-radius: 50%;
              border: 2px solid transparent;
              border-top-color: #00B0E4;
              border-bottom-color: #00B0E4;
              animation: spinner 0.7s ease infinite;
              z-index: 3000;


            }

            @keyframes spinner{
              to{
                transform: rotate(360deg);
              }
            }
        </style>
        
      <style lang="scss">
              body {
                  &.modal-open {
                      bottom: 0;
                      left: 0;
                      position: fixed;
                      right: 0;
                      top: 0;
                  }
          }
      </style>
      
    </head>
    <body class="theme-indigo light layout-fixed">
        <div id="app">
            <div v-cloak>
                <main-app/>
            </div>
            <div v-if="!$data" class="spinner"></div>
        </div>
        
        <script type="text/javascript" src="{{asset(mix('js/all.js'),true)}}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/tinymce/4.7.4/tinymce.min.js" async></script>
        <script type="text/javascript" src="{{asset(mix('js/app.js'),true)}}"></script>
         <script type="text/javascript">
            setTimeout(function() {
                $.holdReady( false );
            }, 2000);
         </script>

    </body>
</html>
