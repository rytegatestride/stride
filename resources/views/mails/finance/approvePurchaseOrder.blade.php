<!DOCTYPE html>
<!-- 


<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title>Stride ERP</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="RyteGate operations manager " name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
     
       
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
      
        <!-- END PAGE LEVEL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="favicon.ico" /> </head>
    <!-- END HEAD -->

    <body class=" login">
        
        <!-- END LOGO -->
        <!-- BEGIN LOGIN -->
        <div class="container">
            <!-- BEGIN LOGIN FORM -->
            <div class="row">
              <div class="col-md-6 well">
            {{$title}} <br>
            </div>
            </div>
            
            <div class="row">
                <div class="col-md-12 well">
                Hi, the Purchase Order for the phase {{$project_phase}} of {{$project}} and contractor {{$contractor}} with vendor {{$vendor}} and the total sum of {{$amount}} has been 

                approved.
               
                <br/><br/>Regards,
                   
                
                </div>
                
            </div>
        </div>
            
          
    </body>

</html>