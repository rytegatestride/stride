<!DOCTYPE html>
<!-- 


<html lang="en">
    <![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title>Stride ERP</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="RyteGate operations manager " name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
     
       
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
      
        <!-- END PAGE LEVEL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="favicon.ico" /> </head>
    <!-- END HEAD -->

    <body class=" login">
        
        <!-- END LOGO -->
        <!-- BEGIN LOGIN -->
        <div class="container">
            <!-- BEGIN LOGIN FORM -->
            <div class="row">
              <div class="col-md-6 well">
             {{$title}} <br>
            </div>
            </div>
            
            <div class="row">
                <div class="col-md-12 well">
                    <?php
                        $request_submitter = \Auth::user()->last_name." ".\Auth::user()->first_name;
                    ?>
            
                     <p> {{$requester}} made a request for the following on {{$date}}: <br>
                        <pre>{!! nl2br(e($container)) !!}</pre><br>
                        The materials requested are needed by {{$needDate}} for Project {{$project}}.<br><br>
                        This requisition has been saved under Purchase Requisition No: PR {{$id}}, submitted by {{ $request_submitter }}<br>

                    </p>
                   
                
                </div>
                
            </div>
        </div>
            
          
    </body>

</html>