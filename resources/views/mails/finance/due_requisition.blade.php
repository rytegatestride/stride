<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>Stride ERP</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="RyteGate operations manager " name="description" />
        <meta content="" name="author" />
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link rel="shortcut icon" href="favicon.ico" /> 
        <style>
            th{
                background-color:#00B0E4;
                text-align:center;
                padding:4px;
            }
            td{
                padding:4px;
            }
            .logo{
                text-align:center;
                padding-bottom:20px;
                text-transform:capitalize;
            }

            li{
                list-style:none;
            }

            .well{
                padding:20px;
            }
        </style>
        
    </head>
    <!-- END HEAD -->

    <body class=" login">
       
        <div class="container">
            <div class="logo">
                <img src="{{asset($company->logo)}}" alt="Logo" width="100"><br>
                <h4 style="color:#00B0E4;">{{$company->name}}</h4>
            </div>
            <div class="row">
                <div class="col-md-12 well">
                    <h4>Due For Payment</h4>
              
                    {!! $content !!}
                   
                </div>
                
            </div>
        </div>
            
          
    </body>

</html>