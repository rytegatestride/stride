<!DOCTYPE html>

    <head>
        <meta charset="utf-8" />
        <title>{{$title}}</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="RyteGate operations manager " name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
     
       
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
      
        <!-- END PAGE LEVEL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="favicon.ico" /> </head>
    <!-- END HEAD -->

    <body class=" login">
        
        <!-- END LOGO -->
        <!-- BEGIN LOGIN -->
        <div class="container">
            <!-- BEGIN LOGIN FORM -->
            <div class="row">
              <div class="col-md-6 well">
              {{ $title }}
             <br>
            </div>
            </div>
            
            <div class="row">
                <div class="col-md-12 well">
                    Hi, {{$user->first_name}} {{ $user->last_name}}
                     <p><b>Your vehicle request from {{date('Y-m-d h:i:s',strtotime($start_date))}} to {{date('Y-m-d h:i:s',strtotime($end_date))}} has been @if($action==1)approved @else Declined @endif</b></p>
                     
                    @if($action==1)
                     <p>you have been assigned to {{$asset->assetmodel->name}} {{$asset->serial_no}}</p>
                     @endif
                </div>
                
            </div>
        </div>
            
          
    </body>

</html>