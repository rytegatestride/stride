<!DOCTYPE html>

    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title>Stride ERP</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="RyteGate operations manager " name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
     
       
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
      
        <!-- END PAGE LEVEL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="favicon.ico" /> </head>
    <!-- END HEAD -->

    <body class=" login">
        
        <!-- END LOGO -->
        <!-- BEGIN LOGIN -->
        <div class="container">
            <!-- BEGIN LOGIN FORM just changes this-->
            <div class="row">
                <div class="col-md-12 well">
              
                     <h4 style="color:green;">Password Reset</h4>
                     <p><b>You are getting this mail because you have requested to change your password</b></p>
                     <p>kindly click the link below to change your password</p>
                     <a href="{{$url}}/reset_password/{{$token}}" target="_blank">Reset Password</a>


                </div>
                
            </div>
        </div>
            
          
    </body>

</html>