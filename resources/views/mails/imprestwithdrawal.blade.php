<!DOCTYPE html>
<!-- 


<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title>Stride ERP</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="RyteGate operations manager " name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
     
       
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
      
        <!-- END PAGE LEVEL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="favicon.ico" /> 

        <style>
            th{
                background-color:#00B0E4;
                text-align:center;
                padding:4px;
            }
            td{
                padding:5px;
            }
        </style>
        
    </head>
    <!-- END HEAD -->

    <body class=" login">
        
        <!-- END LOGO -->
        <!-- BEGIN LOGIN -->
        <div class="container">
           <h4>{{$title}}</h4>
            
            <div class="row">
                <div class="col-md-12 well">
              
                     <h4 style="color:green;">Hi, {{ $user->first_name .' '.$user->last_name }}</h4>
                     has sent the following Imprest withdrawals request which needs your attention: <br>
                     <table class="table">
                        <th>Project</th><th>Material/Service</th><th>Date</th><th>Recipient</th><th>Amount</th><th>Note</th>

                        @foreach($withdrawals as $withdrawal)
                            <tr>
                                <td>{{$withdrawal['project_name']['name']}}</td>
                                <td>{{$withdrawal['material_or_service']['name']}}</td>
                                <td>{{date('Y-m-d H:i',strtotime($withdrawal['date']))}}</td>
                                <td>{{$withdrawal['recipient_name']}}</td>
                                <td>{{$withdrawal['amount']}}</td>
                                <td>{{$withdrawal['notes']}}</td>
                            </tr>
                        @endforeach
                     </table>
                     <br>
                     <br>
                     <a href="{{$url}}/home/user-pending-withdrawals" target="_blank">Click here to review</a>.
                     <br>
                     <br>
                     Regards.
                </div>
                
            </div>
        </div>
            
          
    </body>

</html>