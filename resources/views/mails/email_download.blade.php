<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Email</title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <style>
            th{
                background-color:#00B0E4;
                text-align:center;
                padding:4px;
            }
            td{
                padding:4px;
            }
            .logo{
                text-align:left;
                padding-bottom:20px;
                text-transform:capitalize;
                padding-left:20px;
            }
            #logo{
                width:80px;
                height:80px;
            }

            .well{
                padding:20px;
            }
        </style>
</head>
<body>
<div class="logo">
    <img src="{{public_path().$company->logo}}" id="logo" alt="Logo" width="80" height="80"><br>
    <h4 style="color:#00B0E4;">{{$company->name}}</h4>
</div>
    {!!$data!!}
</body>
</html>