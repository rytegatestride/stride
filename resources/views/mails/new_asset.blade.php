<!DOCTYPE html>
<!-- 


<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title>Stride ERP</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="RyteGate operations manager " name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
     
       
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
      
        <!-- END PAGE LEVEL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="favicon.ico" /> 

        <style>
        th{
            background-color:#00b0e4;
            color:#fff;
            padding-left:10px;
            padding-right:10px;
        }
        td{
            padding-left:10px;
            padding-right:10px;
        }
        table{
            border:solid 1px;
            border-collapse:collapse;
            margin-top:50px;
        }
        tr{
            border:solid 1px;
        }
        td{
            border:solid 1px;
        }
        </style>
        
    </head>
    <!-- END HEAD -->

    <body class=" login">
        
        <!-- END LOGO -->
        <!-- BEGIN LOGIN -->
        <div class="container">
            <!-- BEGIN LOGIN FORM -->
            <div class="row">
              <div class="col-md-6 well">
               
             <br>
            </div>
            </div>
            
            <div class="row">
                <div class="col-md-12 well">
              
                     Hello, the following assets has just been purchased, kindly register them:

                     <table>
                     <th>Name</th><th>Quantity</th><th>Unit Price</th>
                     @foreach($assets as $key=>$asset)
                        <tr>
                            <td>{{$asset["name"]}}</td>
                            <td>{{$asset["quantity"]}}</td>
                            <td>{{$asset["unit_price"]}}</td>
                        </tr>
                    @endforeach
                     </table>

                </div>
                
            </div>
        </div>
            
          
    </body>

</html>