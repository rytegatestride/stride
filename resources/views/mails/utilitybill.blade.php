<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <title>Stride ERP</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="RyteGate operations manager " name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
     
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link rel="shortcut icon" href="favicon.ico" /> 
    </head>
    <!-- END HEAD -->
    <body>
        <div class="container">
            <div class="content">
                <h4><b>Recurrence Expenses Reminder</b></h4>
                
                <div class="row">
                    <div class="col-md-12 well">
                        <p>{!! $info !!}</p>
                        
                    </div>
                    
                </div>
            </div>
        </div> 
    </body>

</html>