<!DOCTYPE html>

    <head>
        <meta charset="utf-8" />
        <title>{{$title}}</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="RyteGate operations manager " name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
     
       
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
      
        <!-- END PAGE LEVEL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="favicon.ico" /> </head>
    <!-- END HEAD -->

    <body class=" login">
        
        <!-- END LOGO -->
        <!-- BEGIN LOGIN -->
        <div class="container">
            <!-- BEGIN LOGIN FORM -->
            <div class="row">
              <div class="col-md-6 well">
              {{ $title }}
             <br>
            </div>
            </div>
            
            <div class="row">
                <div class="col-md-12 well">
                     <p><b>You have been assigned a vehicle with the number {{$asset->serial_no}}</b></p>
                     <p>starting from {{$departure_date}} to {{$arrival_date}}</p>
                     <p>by <b>{{$assigner->first_name}} {{$assigner->last_name}}</b></p>
                </div>
                
            </div>
        </div>
            
          
    </body>

</html>