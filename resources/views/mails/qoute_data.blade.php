<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>{{$title}}</title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <style>
        .rightTD{
            border:1px solid #808080;
            width:170px;
            text-align: center;
        }
        .leftTD{
            text-align: right;
            padding-right: 16px;
        }
        .mycontent{
            padding-top:50px;
            padding-bottom:50px;
            font-size: 12px;
        }
        .title,.name{
            color:#727171;
            text-transform: capitalize;
            
        }
        .name{
            font-size: 22px;
        }
        .title{
            font-size: 22px;
            text-align: center;
            padding-top:0;
        }
        .customer{
            background-color:#808080;
            color:#fff;
            margin-top:20px;
            font-weight: bold;
            padding:5px;
        }
        .item-table{
            width:100%;
            margin-top:40px;
            border-collapse: collapse;
        }
        .item-table td{
            padding-left:6px;
            padding-right: 6px;
        }
        .item-table th{
            background-color: #808080;
            color:#fff;
            font-weight: bold;
            text-align: center;
            padding:5px;
        }
        .right{
            width:15%;
            text-align: right;
            border:1px solid #808080;
            border-left:2px solid #808080;
            border-right:2px solid #808080;
        }
        .center{
            width:10%;
            text-align: center;
            border:1px solid #808080;
            border-left:2px solid #808080;
            border-right:2px solid #808080;
        }
        .left-one,.left-two{
            text-align: left;
            border:1px solid #808080;
            border-left:2px solid #808080;
            border-right:2px solid #808080;
        }
        .right-two{
            background-color: rgba(212, 209, 209, 0.2);
        }
        .left-one{
            width:20%;
        }
        .left-two{
            width:40%;
        }
        .table3{
            width:100%;
        }
        .subtright{
            background-color: rgba(212, 209, 209, 0.2);
            text-align: right;
            border-left: 2px solid rgba(212, 209, 209, 0.2);
            border-right: 2px solid rgba(212, 209, 209, 0.2);
        }
        .subright{
            border: 2px solid rgba(212, 209, 209, 0.2);
            text-align: right;
        }
        .totalright{
            text-align:right;
            background-color: rgba(212, 209, 209, 0.5);
            border-top:3px solid #808080;
        }

        td,th{
            font-size:12px;
        }
        th{
            border-left:2px solid #808080;
            border-right:2px solid #808080;
        }

        .col-xs-1, .col-sm-1, .col-md-1, .col-lg-1, .col-xs-2, .col-sm-2, .col-md-2, .col-lg-2, .col-xs-3, .col-sm-3, .col-md-3, .col-lg-3, .col-xs-4, .col-sm-4, .col-md-4, .col-lg-4, .col-xs-5, .col-sm-5, .col-md-5, .col-lg-5, .col-xs-6, .col-sm-6, .col-md-6, .col-lg-6, .col-xs-7, .col-sm-7, .col-md-7, .col-lg-7, .col-xs-8, .col-sm-8, .col-md-8, .col-lg-8, .col-xs-9, .col-sm-9, .col-md-9, .col-lg-9, .col-xs-10, .col-sm-10, .col-md-10, .col-lg-10, .col-xs-11, .col-sm-11, .col-md-11, .col-lg-11, .col-xs-12, .col-sm-12, .col-md-12, .col-lg-12 {
            border:0;
            padding:0;
            margin-left:-0.00001;
        }
    </style>
</head>
<body>
    <div class="container">
        <div class="content mycontent">
            <div class="row">
                <div class="col-xs-8">
                    <div class="row">
                        <div class="col-xs-2" ><img style="width:100px;height:100px;" src="{{public_path().$logo}}" alt="logo"></div>
                        <div class="col-xs-10 name" style="padding-top:20px;">{{$company}}</div>
                    </div>
                    @if($address1)
                   <div>
                       {{$address1}}
                   </div>
                   @endif
                   @if($address2)
                   <div>
                       {{$address2}}
                    </div>
                    @endif
                    @if($website)
                    <div>
                       {{$website}}
                    </div>
                    @endif
                    @if($phone)
                    <div>
                        {{$phone}}
                    </div>
                    @endif
                </div>
                <div class="col-xs-4">
                    <div class="title" style="padding-top:20px;">
                        {{$title}}
                    </div>
                    <div>
                        <table style="border-collapse: collapse;border:none;">
                            <tr>
                                <td class="leftTD">DATE</td><td class="rightTD">{{$date}}</td>
                            </tr>
                            <tr>
                                <td class="leftTD">QUOTE&nbsp;#</td><td class="rightTD">{{$order_id}}</td>
                            </tr>
                          
                        </table>
                    </div>
                </div>
            </div>

            <!--customer-->
            <div class="row">
                <div class="col-xs-5">
                    <div class="customer">CUSTOMER</div>
                    <div>{{$client_name}}</div>
                    <div>{{$clientcompany}}</div>
                    <div>{{$clientaddress}}</div>
                    <div>{{$country?$country:''}}{{$state?', '.$state:''}}{{$city?', '.$city:''}}</div>
                    <div>{{$clientphone}}</div>
                </div>
            </div>

            <!--body-->
            <div class="row">
                <div class="col-xs-12">
                    <table class="item-table">
                        <tr>
                            <th>PRODUCT</th>
                            <th>DESCRIPTION</th>
                            <th>QTY</th>
                            <th>UNIT PRICE</th>
                            <th>TOTAL</th>
                        </tr>
                       
                        {!! $rows !!}

                        <tr>
                            <td colspan="2" rowspan="6">
                                
                            </td>
                            <td></td>
                            <td style="width:12%;">SUBTOTAL</td>
                            <td style="width:12%;" class="subtright">{!! $symbol !!}{{$subtotal}}</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td style="width:10%;">TAX</td>
                            <td style="width:10%;" class="subright">{!! $symbol !!}{{$tax}}</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td style="width:10%;">SHIPPING</td>
                            <td style="width:10%;" class="subright">{!! $symbol !!}{{$shipping}}</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td style="width:10%;">PAID</td>
                            <td style="width:10%;" class="subright">{!! $symbol !!}{{$paid}}</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td style="width:10%;">BALANCE</td>
                            <td style="width:10%;" class="subright">{!! $symbol !!}{{$balance}}</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td style="width:10%;border-top:3px solid #808080;">TOTAL</td>
                            <td style="width:10%;" class="totalright">{!! $symbol !!}{{$total}}</td>
                        </tr>
                    </table>
                </div>
            </div>

        </div>
    </div>
</body>
</html>