
import moment from 'moment';
import { extendMoment } from 'moment-range';

var schedule;
var holidays;
var ov_code;
var multipliers;
var week_days;

function parseTime(cTime)//Convert html time to milliseconts
{
  let datestring='2019-12-01 '+cTime;
  let newdate=new Date(datestring);
  return newdate;
}
function getTimeDifference(start,end){
   var end = parseTime(end);
   var start = parseTime(start);
   var difference = end - start;
   difference = difference / 1000 / 60 / 60;
   return difference.toFixed(2);
}
function isGreater(time1,time2){
    return parseTime(time1)>parseTime(time2);
}
function getDuration(start,end){
    if(start && end){
        let difference=getTimeDifference(start,end);
        return difference;
    }
    
}
function getDurationBasedOnSchedule(start,end,day_id){
    let sch=schedule.schedule.days.find(item=>item.day_id==(parseInt(day_id)+1));
    
    try{

        if(sch){
           
            let schedule_duration=getTimeDifference(sch.start_time,sch.end_time);
    
            if(start && end){
                
                let schedule_start=sch.start_time;
                let schedule_end=sch.end_time;
    
                let actual_start='';
                let actual_end='';
                let excess='';
                let excess2='';
                let excess_interval='';
                let excess2_interval='';
                let normal_interval='';
    
                if(isGreater(schedule_start,start)){
                    actual_start=schedule_start;
                    excess2=getTimeDifference(start,schedule_start);
                    excess2_interval=moment(parseTime(start)).format('hh:mma')+' to '+moment(parseTime(schedule_start)).format('hh:mma');
                }
                else{
                    actual_start=start;
                }
    
                if(isGreater(end,schedule_end)){
                    actual_end=schedule_end;
                    excess=getTimeDifference(schedule_end,end);
                    excess_interval=moment(parseTime(schedule_end)).format('hh:mma')+' to '+moment(parseTime(end)).format('hh:mma');
                }
                else{
                    actual_end=end;
                }
    
                let total_excess=((excess?parseFloat(excess):0)+(excess2?parseFloat(excess2):0)).toFixed(2);
    
                let difference=getTimeDifference(actual_start,actual_end);
                normal_interval=moment(parseTime(actual_start)).format('hh:mma')+' to '+moment(parseTime(actual_end)).format('hh:mma');
                let realDuration={duration:difference,excess:total_excess,excess_interval:excess_interval,excess2_interval:excess2_interval,
                normal_interval:normal_interval,excess_one:excess,excess_two:excess2,schedule_duration};
                return realDuration;
    
            }
            else{
                let realDuration={duration:0,excess:0,excess_interval:'',excess2_interval:'',
                normal_interval:'',excess_one:'',excess_two:'',schedule_duration};
                return realDuration;
            }
        }
        else{
        
            return '';
        }

    }catch(error){
        
    }
    
}


function get_leave(day,leaves,week_date){
    let attendance={};
    try{
        let leave=leaves.find(item=>moment.utc(item.date).format('YYYY-MM-DD') ==moment.utc(week_date).format('YYYY-MM-DD'));
    if(leave){
        let start=new Date(leave.start_date);
        let end=new Date(leave.end_date);
        if((start-end)!=0){
                   
            let sch=schedule.schedule.days.find(item=>item.day_id===day);
            let schedule_start=sch.start_time;
            let schedule_end=sch.end_time;
            attendance.leave_interval=moment(parseTime(sch.start_time)).format('HH:mma')+' to '+moment(parseTime(sch.end_time)).format('HH:mma');

            attendance.vacation=getDuration(schedule_start,schedule_end);
            let mult=multipliers.find(item=>item.factor_id==leave.leave_type_id);
            attendance.vac_multiplier=mult.code;
            attendance.schedule_duration=attendance.vacation;
        }
        else{
            
            let sch=schedule.schedule.days.find(item=>item.day_id===day);
            let date1=parseTime(leave.start_time);
            let date2=parseTime(leave.end_time);
            let schedule_start=leave.start_time;
            let schedule_end=leave.end_time;
            
            attendance.leave_interval=moment(new Date(date1)).format('hh:mma')+' to '+moment(new Date(date2)).format('hh:mma');
           
            attendance.vacation=getDuration(schedule_start,schedule_end);
            let mult=multipliers.find(item=>item.factor_id==leave.leave_type_id);
            attendance.vac_multiplier=mult.code;
            attendance.schedule_duration=getDuration(sch.start_time,sch.end_time);
        }

        return attendance;
    }
    }catch(error){
        console.log(error);
    }

}

function get_holiday(holies,schedule,day,mult){

    let sch=schedule.schedule.days.find(sc=>sc.day_id===day);
  
    if(sch){
        let holi=holies.find(item=>(parseInt(item.day_id)+1)===day);
        if(holi){
            let attendance={
                punchedin:'',
                punchedout:'',
                day_id:holi.day_id,
                date:moment(holi.day).format('dddd Do MM YYYY'),
                date2:moment(holi.day).format('YYYY-MM-DD')
            }
            attendance.duration=getDuration(sch.start_time,sch.end_time);
            attendance.multiplier=mult.code;
            attendance.interval=moment(parseTime(sch.start_time)).format('HH:mma')+' to '+moment(parseTime(sch.end_time)).format('HH:mma');
            attendance.holiday=1;
            return attendance;
        }
    }
}


export function parseAttendance(attendances,sc,leaves,holi,multi,month_days){
    schedule=sc;
    holidays=holi;
    multipliers=multi;
    week_days=month_days;

    try{
    let multiplier=multipliers.find(item=>item.type==3);
    let ov_code=multipliers.find(item=>item.type==2);

        //try using the days of the week
        week_days.forEach(week_day=>{
            let attendance=attendances.find(item=>(((parseInt(item.day_id)+1)===week_day.day) || (parseInt(item.day_id)+1===7) && week_day.day===0));
            
                if(attendance){
                    let punchedin=attendance.punchedin?moment(attendance.punchedin).format('HH:mm'):'';
                    let punchedout=attendance.punchedout?moment(attendance.punchedout).format('HH:mm'):'';
                    attendance.in=punchedin;
                    attendance.out=punchedout;
                    let scheduleTime=getDurationBasedOnSchedule(punchedin,punchedout,attendance.day_id);
                    if(scheduleTime){
                    
                        attendance.duration=scheduleTime.duration;
                        attendance.multiplier=multiplier.code;
                        attendance.excess=scheduleTime.excess;
                        attendance.excess_one=scheduleTime.excess_one;
                        attendance.excess_two=scheduleTime.excess_two;
                        attendance.excess_interval=scheduleTime.excess_interval;
                        attendance.excess2_interval=scheduleTime.excess2_interval;
                        attendance.normal_interval=scheduleTime.normal_interval;
                        attendance.ov_code=ov_code.code;
                        attendance.schedule_duration=scheduleTime.schedule_duration;
                    }
                    else{
                    
                        if(holidays.find(holi=>holi.day_id==attendance.day_id)){
                            
                        }
                        else{
                            attendance.duration=0;
                            attendance.not_on_schedule=true;
                            attendance.excess=getDuration(punchedin,punchedout);
                            attendance.multiplier=null;
                            attendance.ov_code=ov_code.code;
                            
                        }
                    }
                    week_day.date2=moment(week_day.date).format('dddd Do MM YYYY');
                    week_day.date=moment(week_day.date).format('YYYY-MM-DD');
                    week_day.attendance=attendance;
                    
                }
                else{
                    
                }
            

            //check for leave
            week_day.leave=get_leave(week_day.day,leaves,week_day.date);

            //get holidays
            week_day.holiday=get_holiday(holidays,schedule,week_day.day,multiplier);

        });
   


     //get all days that have been assigned already
     let day_ids=[];
     week_days.forEach(data=>{
         if(!data.attendance && !data.leave && ! data.holiday){
             day_ids.push(data.day);
         }
     })

     let missing_days=[];
   
     schedule.schedule.days.forEach(data=>{
         let found=day_ids.find(item=>item===data.day_id);
         if(found){
             missing_days.push(data);
         }
     })

     //handle the missing days
     week_days.forEach(data=>{
         let sc=missing_days.find(item=>item.day_id===data.day);
         if(sc){
             let attendance={};
             let duration=getDuration(sc.start_time,sc.end_time);
             attendance.duration=duration;
             attendance.punchedin=sc.start_time;
             attendance.punchedout=sc.end_time;
             attendance.code='';
             attendance.interval=moment(parseTime(sc.start_time)).format('HH:mma')+' to '+moment(parseTime(sc.end_time)).format('HH:mma');
             data.missed=attendance;
         }
     })

     //mark others as not applicable
     week_days.forEach(data=>{
         data.more=[];
        if(!data.attendance && !data.leave && !data.holiday && !data.missed){
            data.not_applicable=true;
        }
     })

    }
    catch(e){
        console.log(e);
    }

     return week_days;

}



export function reParseData(attendances,sc){
    schedule=sc;
    try{
        attendances.forEach(attendance=>{

            if(attendance.attendance){
                let punchedin=attendance.attendance.punchedin?moment(attendance.attendance.punchedin).format('HH:mm'):'';
                let punchedout=attendance.attendance.punchedout?moment(attendance.attendance.punchedout).format('HH:mm'):'';
                attendance.in=punchedin;
                attendance.out=punchedout;
                let scheduleTime=getDurationBasedOnSchedule(punchedin,punchedout,attendance.attendance.day_id);
                if(scheduleTime){
                    attendance.attendance.duration=scheduleTime.duration;
                    attendance.attendance.excess=scheduleTime.excess;
                    attendance.attendance.excess_one=scheduleTime.excess_one;
                    attendance.attendance.excess_two=scheduleTime.excess_two;
                    attendance.attendance.excess_interval=scheduleTime.excess_interval;
                    attendance.attendance.excess2_interval=scheduleTime.excess2_interval;
                    attendance.attendance.normal_interval=scheduleTime.normal_interval;
                }
            }
                
        })
    }catch(error){
       
    }
    
    return attendances;
}