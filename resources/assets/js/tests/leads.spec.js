
import { createLocalVue, shallowMount } from '@vue/test-utils'
import TempComponent from '@/components/Temp.vue'
import { Temp } from '@/components/Temp'
import VueRouter from 'vue-router'
import Leads from '../components/sales-hub/Leads.vue'

const localVue = createLocalVue()
localVue.use(VueRouter)

describe("Leads.vue", () => {
  const wrapper = shallowMount(Leads,{
    stubs: ['router-link']
  });
  expect(wrapper.isVueInstance).toBeTruthy();

});