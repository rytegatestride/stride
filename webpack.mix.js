let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
mix.config.publicPath='public_html';

mix.js('resources/assets/js/app.js', 'public_html/js')
   .sass('resources/assets/sass/app.scss', 'public_html/css');

mix.styles([
   'public_html/css/bootstrap.min.css',
   'public_html/css/layout.min.css',
   'public_html/css/style.css',
   'public_html/css/main_theme.min.css',
 
   'public_html/css/component.min.css',
   'public_html/css/waves.min.css',
   'public_html/css/animate.css',
   'public_html/css/spinkit.css',
   'public_html/css/unslider.css',
], 'public_html/css/all.css');

mix.scripts([
    'public_html/js/jquery.min.js',
    'public_html/js/jquery.slimscroll.js',
    'public_html/js/jquery.storageapi.js',
    'public_html/js/modernizr.custom.js',
    'public_html/js/bootstrap-notify.js',
    'public_html/js/classie.js',
    'public_html/js/modalEffects.js',
   
    'public_html/js/waves.js',
    'public_html/js/demo.js',
     'public_html/js/layout.js'
     ], 'public_html/js/all.js');

// 'public_html/js/modals.js',
mix.options({
   uglifyOptions: {
  compress: false
   }
});

mix.version(); 

